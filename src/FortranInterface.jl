using .MPI 

const libvmec_ref = Ref{String}("")
const libvmec_dlopened = Ref{Bool}(false)

# This code block runs when the MPI is available
if Sys.iswindows()
    @warn "ScaLAPACK is not available on Windows, running VMEC is not possible"
else
  if occursin("Intel",first(Sys.cpu_info()).model) || haskey(ENV, "MKLROOT")
    try
      libvmec_ref[] = dlopen(libvmec_mkl; throw_error = false) !== nothing ? libvmec_mkl : libvmec_openblas
    catch
      @warn "Cannot open libvmec from VMEC_jll"
    end
  else
    libvmec_ref[] = libvmec_openblas
  end
  try
    ccall((:__vmec_ext_interface_MOD_initialize, libvmec_ref[]), Nothing, (Ptr{Int32}, Ptr{Int32}), [4], [4])
    ccall((:__vmec_ext_interface_MOD_finalize, libvmec_ref[]), Nothing, ())
    libvmec_dlopened[] = true
  catch
    @warn "Unable to call VMEC initialize/finalize. Runnng VMEC is not possible"
  end
end

function load_vmec_lib(libpath::AbstractString)
  libvmec_ref[] = libpath
  try
    libvmec_state = libvmec_dlopened[]
    ccall((:__vmec_ext_interface_MOD_initialize, libvmec_ref[]), Nothing, (Ptr{Int32}, Ptr{Int32}), [4], [4])
    ccall((:__vmec_ext_interface_MOD_finalize, libvmec_ref[]), Nothing, ())
    libvmec_dlopened[] = true
    if !libvmec_state
      @info "VMEC successfully called, running VMEC is possible"
    end
  catch
    @warn "Unable to call VMEC initialize/finalize. Runnng VMEC currently not possible"
  end
end

"""
    build_boundary_coeffs(nml::AbstractDict{Symbol,Any},keyString::String)

Generate the appropriate arrays of boundary coefficients from the `nml`
namelist dictionary.  Possible values of `keyString` are "rbc", "zbs",
"rbs" (asymmetric) and "zbc" (asymmmetric).
"""
function build_boundary_coeffs(namelistParameters::AbstractDict{Symbol,Any},
                               keyString::String;
                              )
  mpol = namelistParameters[:mpol]
  ntor = namelistParameters[:ntor]
  dataSize = (2*ntor+1)*mpol
  data = zeros(Float64,dataSize)

  m = 0
  for n = 0:ntor
    key = Symbol(keyString*"("*string(n)*","*string(m)*")")
    if haskey(namelistParameters,key)
      offset = ntor+ 1 + n
      data[offset] = namelistParameters[key]
    end
  end
  for m = 1:mpol-1
    for n = -ntor:ntor
      key = Symbol(keyString*"("*string(n)*","*string(m)*")")
      if haskey(namelistParameters,key)
        offset = (2*ntor+1)*m + ntor + 1 + n
        data[offset] = namelistParameters[key]
      end
    end
  end
  return data
end

"""
    set_vmec_data(nml::AbstractDict{Symbol,Any},libsyms::Dict{Symbol,Ptr{Nothing}})

Set the internal values of the VMEC variables specified by the `nml`
namelist using the library symbols given by `libsyms`.

See also: [`getLibSymbols`](@ref)
"""
function set_vmec_data(runtimeParameters::AbstractDict{Symbol,Any})
  if libvmec_dlopened[]
    for (key, value) in runtimeParameters
      if !occursin(r"bc\(",string(key)) && !occursin(r"bs\(",string(key))
        T = eltype(VmecInputTypes[key])
        dataid = [string(key)]
        data = !(typeof(value) <: Array) ? !(typeof(value) <: String) ? [convert(T,value)] : [value] : convert.(T,value)
        dataSize = [convert(Int32,length(data))]
        if T === Bool
          ccall((:__vmec_ext_interface_MOD_set_vmec_data_bool, libvmec_ref[]), Nothing, (Ptr{Int32}, Ptr{Ptr{UInt8}}, Ptr{Int32}), dataSize, dataid, data)
        elseif T === Int32
          ccall((:__vmec_ext_interface_MOD_set_vmec_data_int, libvmec_ref[]), Nothing, (Ptr{Int32}, Ptr{Ptr{UInt8}}, Ptr{Int32}), dataSize, dataid, data)
        elseif T === Float64
          ccall((:__vmec_ext_interface_MOD_set_vmec_data_real, libvmec_ref[]), Nothing, (Ptr{Int32}, Ptr{Ptr{UInt8}}, Ptr{Float64}), dataSize, dataid, data)
        elseif T === Char
          ccall((:__vmec_ext_interface_MOD_set_vmec_data_char, libvmec_ref[]), Nothing, (Ptr{Int32}, Ptr{Ptr{UInt8}}, Ptr{Ptr{UInt8}}), dataSize, dataid, data)
        else
          throw(ArgumentException("Cannot convert VMEC input data"))
        end
      end
    end
    rbc = build_boundary_coeffs(runtimeParameters, "rbc")
    dataid = ["rbc"]
    dataSize = [convert(Int32, length(rbc))]
    ccall((:__vmec_ext_interface_MOD_set_vmec_data_real, libvmec_ref[]), Nothing, (Ptr{Int32}, Ptr{Ptr{UInt8}}, Ptr{Float64}), dataSize, dataid, rbc)
    zbs = build_boundary_coeffs(runtimeParameters, "zbs")
    dataid = ["zbs"]
    dataSize = [convert(Int32, length(zbs))]
    ccall((:__vmec_ext_interface_MOD_set_vmec_data_real, libvmec_ref[]), Nothing, (Ptr{Int32}, Ptr{Ptr{UInt8}}, Ptr{Float64}), dataSize, dataid, zbs)
    if (runtimeParameters[:lasym])
      rbs = build_boundary_coeffs(runtimeParameters, "rbs")
      dataid = ["rbs"]
      dataSize = [convert(Int32, length(rbs))]
      ccall((:__vmec_ext_interface_MOD_set_vmec_data_real, libvmec_ref[]), Nothing, (Ptr{Int32}, Ptr{Ptr{UInt8}}, Ptr{Float64}), dataSize, dataid, rbs)
      zbc = build_boundary_coeffs(runtimeParameters, "zbc")
      dataid = ["zbc"]
      dataSize = [convert(Int32, ength(zbc))]
      ccall((:__vmec_ext_interface_MOD_set_vmec_data_real, libvmec_ref[]), Nothing, (Ptr{Int32}, Ptr{Ptr{UInt8}}, Ptr{Float64}), dataSize, dataid, zbc)
    end
  end
end

"""
    set_vmec_field!(vmec::VmecData,field::Symbol,data::Array{T,1}) where T

Set the field `field` of the `VmecData` object `vmec` using the data returned
from Fortran in the 1D array `data`.  The array is reshaped to match the required
size determined by `field`.
"""
function set_vmec_field!(vmec::VmecData,
                         field::Symbol,
                         data::Array{T,1};
                        ) where {T}
  fieldSize = size(getfield(vmec, field))
  fieldLength = length(getfield(vmec, field))
  if fieldLength == 1
    setfield!(vmec, field,data[1])
  else
    setfield!(vmec, field, reshape(data[1:fieldLength], fieldSize))
  end
end

"""
    retrieve_vmec_data(libSymbols::Dict{Symbol,Ptr{Nothing}})

Retrieve the data stored in-memory from a finished VMEC calculation.  Returns
a `Vmec` and `VmecData` object.
"""
function retrieve_vmec_data()
  if libvmec_dlopened[]
    ns = Vector{Int32}(undef,1)
    mnmax = Vector{Int32}(undef,1)
    mnmax_nyq = Vector{Int32}(undef,1)
    dataSize = [convert(Int32,1)]
    dataId = ["ns"]
    ccall((:__vmec_ext_interface_MOD_retrieve_vmec_data_int, libvmec_ref[]), Nothing,(Ptr{Int32}, Ptr{Ptr{UInt8}}, Ptr{Int32}), dataSize, dataId,ns)
    ns = ns[1]
    dataId = ["mnmax"]
    ccall((:__vmec_ext_interface_MOD_retrieve_vmec_data_int, libvmec_ref[]), Nothing, (Ptr{Int32}, Ptr{Ptr{UInt8}}, Ptr{Int32}), dataSize, dataId, mnmax)
    mnmax = mnmax[1]
    dataId = ["mnmax_nyq"]
    ccall((:__vmec_ext_interface_MOD_retrieve_vmec_data_int, libvmec_ref[]), Nothing, (Ptr{Int32}, Ptr{Ptr{UInt8}}, Ptr{Int32}), dataSize, dataId, mnmax_nyq)
    mnmax_nyq = mnmax_nyq[1]
    lasym = Vector{Bool}(undef,1)
    dataId = ["lasym"]
    ccall((:__vmec_ext_interface_MOD_retrieve_vmec_data_bool, libvmec_ref[]), Nothing, (Ptr{Int32}, Ptr{Ptr{UInt8}}, Ptr{Bool}), dataSize, dataId, lasym)
    lasym =lasym[1]
    vmec = VmecData(ns, mnmax, mnmax_nyq, lasym)

    dataSize = [ns * (mnmax_nyq > mnmax ? mnmax_nyq : mnmax)]
    for (name, info) in VmecWoutInfo
      T = eltype(info.datatype)
      lasym_flag = info.lasym
      dataId = [string(name)]
      if ((lasym && lasym_flag) || !lasym_flag) && T != Char
        data = zeros(T, dataSize[1])
        if T === Bool
          ccall((:__vmec_ext_interface_MOD_retrieve_vmec_data_bool, libvmec_ref[]), Nothing, (Ptr{Int32}, Ptr{Ptr{UInt8}}, Ptr{Bool}), dataSize, dataId, data)
        elseif T === Int32
          ccall((:__vmec_ext_interface_MOD_retrieve_vmec_data_int, libvmec_ref[]), Nothing, (Ptr{Int32}, Ptr{Ptr{UInt8}}, Ptr{Int32}), dataSize, dataId, data)
        elseif T === Float64
          ccall((:__vmec_ext_interface_MOD_retrieve_vmec_data_real, libvmec_ref[]), Nothing, (Ptr{Int32}, Ptr{Ptr{UInt8}}, Ptr{Float64}), dataSize, dataId, data)
        else
          throw(ArgumentError("Cannot retrieve vmec data with id $(dataId)"))
        end
        set_vmec_field!(vmec, name, data)
      end
    end
    return Vmec(vmec)
  else
    @warn "Attemping to call retrieve_vmec_data but libvmec is not dlopened!"
    return nothing
  end
end

"""
    run_vmec([comm=MPI.COMM_WORLD],nml::AbstractDict{Symbol,Any},outputFile=nothing)

Run the VMEC code using the MPI communicator `comm` and the input namelist
parameters given by the `nml` dictionary, returning `Vmec` and `VmecData` objects.
If VMEC does not find a converged solution for the input given in `nml`, the tuple
  `(nothing, nothing)` is returned.

See also: [`readVmecNamelist`](@ref)
"""
function run_vmec(comm::MPI.Comm,
                  namelistParameters::AbstractDict{Symbol,Any},
                  outputFile=nothing;
                 )
  if libvmec_dlopened[] && MPI.Initialized()
    rank = MPI.Comm_rank(comm)
    mpol = [convert(Int32, namelistParameters[:mpol])]
    ntor = [convert(Int32, namelistParameters[:ntor])]
    ccall((:__vmec_ext_interface_MOD_initialize, libvmec_ref[]), Nothing, (Ptr{Int32}, Ptr{Int32}), mpol, ntor)
    set_vmec_data(namelistParameters)
    writeFileName = isnothing(outputFile) ? [""] : [outputFile]
    #MPI.Barrier(comm)
    fcomm = [comm.val]
    stat = Vector{Bool}(undef,1)
    ccall((:__vmec_ext_interface_MOD_runvmec_ext, libvmec_ref[]), Nothing, (Ptr{Ptr{UInt8}}, Ptr{UInt32}, Ptr{Bool}), writeFileName, fcomm, stat)
    # Only the head node has the accurate success code
    stat = MPI.bcast(stat, 0, comm)
    success = stat[1]
    if rank == 0
      vmec = success ? retrieve_vmec_data() : nothing
    else
      vmec = nothing
    end
    ccall((:__vmec_ext_interface_MOD_finalize, libvmec_ref[]), Nothing, ())
    if success
      vmec = MPI.bcast(vmec,0,comm)
    end
    return vmec
  else
    @warn "Attempting to call run_vmec but libvmec is not dlopened!"
    return nothing
  end
end

function run_vmec(namelistParameters::AbstractDict{Symbol, Any},
                  outputFile = nothing;
                 )
  !MPI.Initialized() && MPI.Init()
  return run_vmec(MPI.COMM_WORLD, namelistParameters, outputFile)
end
