module VMEC
using Libdl
using VMEC_jll
using Requires
using HDF5
using NetCDF
using OrderedCollections
using LinearAlgebra
using StaticArrays
using StructArrays
using OrderedCollections
using CoordinateTransformations
using Interpolations
using Roots
using Optim
using Polyester


using PlasmaEquilibriumToolkit

include("Exports.jl")
include("VmecTypes.jl")
include("NamelistInput.jl")
include("NetCDFUtils.jl")
include("VmecUtils.jl")
include("Coordinates.jl")
include("Boozer.jl")
include("utilities.jl")
include("Output.jl")


function __init__()
  @require MPI="da04e1cc-30fd-572f-bb4f-1f8673147195" begin
    include("FortranInterface.jl")
  end

  @require StellaratorOptimization="f2f44f23-99b2-4d7c-931e-31858dc08d61" begin
    @require MPI="da04e1cc-30fd-572f-bb4f-1f8673147195" include("optimization/StellOptInterface.jl")
  end
end

end
