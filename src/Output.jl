
function writeEqH5(vmec::VmecData,filename::String)
  fid = h5open(filename,"w")
  g_create(fid,"vmec")
  gid = fid["vmec"]
  entries = propertynames(vmec)
  for entry in entries
    stringId = String(entry)
    try
      data = getfield(vmec,entry)
      write(gid,stringId,data)
    catch
    end
  end
  close(fid)
end

"""
    sanitizeString(s::AbstractString)

Return the appropriate string values for use in a Fortran namelist
"""
function sanitizeString(s::AbstractString)
  #check for [ characters
  if occursin(r"\[",s)
    s = replace(s,"["=>"")
    s = replace(s,"]"=>"")
  end
  if occursin(r",",s)
    s = replace(s,","=>"")
  end
  #check for empty strings
  if length(s) == 0
    return "''"
  end
  #check for strings
  if isletter(s[1])
    if s == "false"
      return "F"
    elseif s == "true"
      return "T"
    else
      return "'"*s*"'"
    end
  end

  return s
end

"""
    writeVmecInput(namelistDict::AbstractDict{Symbol,Any}, filename::String)

Write a dictionary representing the VMEC input to a traditional Fortran namelist
file labeled by `filename`, allowing for independent calculation of the VMEC.

See also: [`readVmecNamelist`](@ref)
"""
function writeVmecInput(namelistDict::AbstractDict{Symbol,Any},
                        filename::String;
                       )
    fid = open(filename, "w")
    write(fid,"&INDATA\n")
    for (key, value) in namelistDict
        write(fid,"  ")
        write(fid,key)
        write(fid," = ")
        newString = sanitizeString(string(value))
        write(fid,newString)
        write(fid,"\n")
        #end
    end
    write(fid,"/\n")

    close(fid)
end

