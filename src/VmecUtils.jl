function PlasmaEquilibriumToolkit.grad_B(x::VmecCoordinates,
                                         vmec::VmecSurface{T};
                                        ) where {T}
  e_x = basis_vectors(Covariant(), CartesianFromVmec(), x, vmec)
  return grad_B(x, e_x, vmec)
end

function PlasmaEquilibriumToolkit.grad_B(x::PestCoordinates,
                                         vmec::VmecSurface{T};
                                        ) where {T}
  y = VmecFromPest()(x, vmec)
  return grad_B(y, vmec)
end

function PlasmaEquilibriumToolkit.grad_B(x::FluxCoordinates,
                                         vmec::VmecSurface{T};
                                        ) where {T}
y = VmecFromFlux()(x, vmec)
return grad_B(y, vmec)
end

function PlasmaEquilibriumToolkit.grad_B(x::VmecCoordinates,
                                         ev::BasisVectors{T},
                                         vmec::VmecSurface{T};
                                        ) where {T}
  dBds = inverseTransform(x,vmec.bmn;deriv=:ds)
  dBdθ = inverseTransform(x,vmec.bmn;deriv=:dθ)
  dBdζ = inverseTransform(x,vmec.bmn;deriv=:dζ)
  return ev[:,1]*dBds .+ ev[:,2]*dBdθ .+ ev[:,3]*dBdζ
end

function PlasmaEquilibriumToolkit.grad_B(x::PestCoordinates,
                                         ev::BasisVectors{T},
                                         vmec::VmecSurface{T};
                                        ) where {T}
    y = VmecFromPest()(x, vmec)
    return grad_B(y, ev, vmec)
end

function PlasmaEquilibriumToolkit.grad_B(x::FluxCoordinates,
                                         ev::BasisVectors{T},
                                         vmec::VmecSurface{T};
                                        ) where {T}
    y = VmecFromFlux()(x, vmec)
    return grad_B(y, ev, vmec)
end

function PlasmaEquilibriumToolkit.B_norm(x::VmecCoordinates,
                                         vmec::VmecSurface{T};
                                        ) where {T}
  return inverseTransform(x,vmec.bmn)
end

function PlasmaEquilibriumToolkit.B_norm(x::PestCoordinates,
                                         vmec::VmecSurface{T};
                                        ) where {T}
    b = B_field(x, vmec)
    return norm(b, 2)
end

function PlasmaEquilibriumToolkit.B_norm(x::FluxCoordinates,
                                         vmec::VmecSurface{T};
                                        ) where {T}
    b = B_field(x, vmec)
    return norm(b, 2)
end

function PlasmaEquilibriumToolkit.B_field(x::VmecCoordinates,
                                          vmec::VmecSurface{T};
                                         ) where {T}
  ∇x = basis_vectors(Contravariant(), CartesianFromVmec(), x, vmec)
  return B_field(Contravariant(), ∇x)
end

function PlasmaEquilibriumToolkit.B_field(x::PestCoordinates,
                                          vmec::VmecSurface{T};
                                         ) where {T}
    ∇x = basis_vectors(Contravariant(), CartesianFromPest(), x, vmec)
    return cross(∇x[:, 1], ∇x[:, 2])
end

function PlasmaEquilibriumToolkit.B_field(x::FluxCoordinates,
                                          vmec::VmecSurface{T};
                                         ) where {T}
    ∇x = basis_vectors(Contravariant(), CartesianFromFlux(), x, vmec)
    return cross(∇x[:,1], ∇x[:,2] - vmec.iota[1] * ∇x[:,3])
end

function PlasmaEquilibriumToolkit.jacobian(x::VmecCoordinates,
                                           vmec::VmecSurface{T};
                                          ) where {T}
  return inverseTransform(x, vmec.gmn)
end

function PlasmaEquilibriumToolkit.jacobian(x::PestCoordinates,
                                           vmec::VmecSurface{T};
                                          ) where {T}
    ∇x = basis_vectors(Contravariant(), CartesianFromPest(), x, vmec)
    return jacobian(Contravariant(), ∇x)
end

function PlasmaEquilibriumToolkit.jacobian(x::FluxCoordinates,
                                           vmec::VmecSurface{T};
                                          ) where {T}
    ∇x = basis_vectors(Contravariant(), CartesianFromFlux(), x, vmec)
    return jacobian(Contravariant(), ∇x)
end
