using .MPI
using .StellaratorOptimization

function StellOpt.EquilibriumWrapper{T, E, D}(nml::AbstractDict,
                                              grad_method::D = StellOpt.SPSA();
                                             ) where  {T, E <: Vmec, D <: StellOpt.AbstractDerivative}
    ns = last(nml[:ns_array])
    m_pol = nml[:mpol]
    n_tor = nml[:ntor]
    n_theta = haskey(nml, :ntheta) ? div(nml[:ntheta], 2) : div(2 * m_pol + 6, 2)
    m_nyq = max(2 * n_theta, 2 * (m_pol + 1))
    n_nyq = max(2 * div(nml[:nzeta], 2), 2 * n_tor)
    mnmax_nyq = div(n_nyq, 2) + 1 + div(m_nyq * (n_nyq + 1), 2)
    mnmax = n_tor + 1 + (m_pol - 1) * (1 + 2 * n_tor)
    lasym = nml[:lasym]
    vmec_data = VmecData(ns, mnmax, mnmax_nyq, lasym; FT = T)
    translator = Dict{Int, Symbol}()
    geometries = Dict{UInt, Expr}()
    return StellOpt.EquilibriumWrapper{T, E, D}(Vmec(vmec_data), nml, translator, geometries, grad_method)
end
function StellOpt.EquilibriumWrapper{T, E, D}(nml_file::AbstractString,
                                              grad_method::D = StellOpt.SPSA();
                                       ) where  {T, E <: Vmec, D <: StellOpt.AbstractDerivative}
    nml_dict = read_vmec_namelist(nml_file)
    return StellOpt.EquilibriumWrapper{T, E, D}(nml_dict, grad_method)
end

function StellOpt.EquilibriumWrapper(nml::AbstractDict,
                                     ::Type{E},
                                     grad_method::D = StellOpt.SPSA();
                                    ) where {E <: Vmec, D <: StellOpt.AbstractDerivative}
    T = typeof(nml[Symbol("rbc(0,1)")])
    return StellOpt.EquilibriumWrapper{T, E, D}(nml, grad_method)
end

function StellOpt.EquilibriumWrapper(nml::AbstractDict,
                                     vmec::E,
                                     grad_method::D = StellOpt.SPSA();
                                    ) where {E <: Vmec, D <: StellOpt.AbstractDerivative}
    return StellOpt.EquilibriumWrapper{T, E, D}(vmec, nml, Dict{Int, Symbol}(), Dict{UInt, Expr}(), grad_method)
end

function StellOpt.EquilibriumWrapper(vmec::E,
                                     grad_method::D = StellOpt.SPSA();
                                    ) where {E <: Vmec, D <: StellOpt.AbstractDerivative}
    nml = OrderedDict{Symbol, Any}()
    vmec_data = VmecData(vmec)
    update_input_namelist!(nml, vmec_data)
    return StellOpt.EquilibriumWrapper{T, E, D}(vmec, nml, Dict{Int, Symbol}(), Dict{UInt, Expr}(), grad_method)
end

function StellOpt.compute_equilibrium(wrapper::StellOpt.EquilibriumWrapper{T, E, D},
                                      comm::MPI.Comm;
                                     ) where {T, E <: Vmec, D <: StellOpt.AbstractDerivative}
    return run_vmec(comm, wrapper.input)
end

function StellOpt.translate!(wrapper::StellOpt.EquilibriumWrapper{T, E, D},
                             state_vector::Vector{V};
                            ) where {T, E <: Vmec, D <: StellOpt.AbstractDerivative, V <: StellOpt.AbstractOptVariable}
    for sv in state_vector
        if haskey(VmecInputTypes, sv.name)
            wrapper.input[sv.name] = sv.value
        end
    end
end

function StellOpt.translate!(wrapper::StellOpt.EquilibriumWrapper{T, E, D},
                             state_vector::Vector{V},
                             comm::MPI.Comm;
                            ) where {T, E <: Vmec, D <: StellOpt.AbstractDerivative, V <: StellOpt.AbstractOptVariable}
    rank = MPI.Comm_rank(comm)
    new_input = empty(wrapper.input)
    if rank == 0
        StellOpt.translate!(wrapper, state_vector)
        MPI.bcast(wrapper.input, 0, comm)
    else
        new_input = MPI.bcast(new_input, 0, comm)
        StellOpt.set_input!(wrapper, new_input)
    end
end


"""
    add_variable!(prob::Problem, )

Construct an unconstrained optimization variable from a VMEC input
variable. The initial value is read from the VmecOpt.vmecInput dictionary
and is set to zero if no value is found.  Optional variables are given by
keyword arguments.

# Arguments:
- diffType # Derivative type, supported keywords are AutoForwardDiff, AutoReverseDiff (for AD) and CenteredDiff, ForwardDiff (for finite difference approximations)
- fDiffStep # Size of the finite difference step, set to 1% of current value if not specified
- weight # Weighting of the variable, currently not used

# Example
```julia-repl
julia> using StellOpt, VMEC

julia> prob = StellOpt.Problem{Float64}()

julia> add_vmec_variable!(prob, "rbc(0,1)", finite_diff_step=0.001);

julia> prob.state_vector[end].name === Symbol("rbc(0,1)")
true
```
"""
function StellOpt.add_variable!(prob::StellOpt.Problem,
                                wrapper::StellOpt.EquilibriumWrapper{T, E, D},
                                name::Union{AbstractString, Symbol};
                                kwargs...
                               ) where {T, E <: Vmec, D <: StellOpt.AbstractDerivative}
    if typeof(name) <: AbstractString
        name_string = replace(lowercase(name), " " => "")
        symbol_name = Symbol(name_string)
    else
        symbol_name = name
    end

    var_value = zero(T)
    if haskey(wrapper.input, symbol_name)
        var_value = wrapper.input[symbol_name]
    #else
        #@warn "Vmec variable $(name_string) has no specified initial value, setting to 0"
    end

    index = StellOpt.add_variable!(prob,
                                  StellOpt.OptimizationVariable(symbol_name, var_value,
                                                                objectid(wrapper); kwargs...))
    StellOpt.update_translator!(wrapper, index, symbol_name)
    return nothing
end


"""
    add_multiple_modes!(prob, wrapper, min_n, max_n, max_m; symmetric=true)

Adds rbc and zbs modes from `n` from `min_n` to `max_n` and `m` from `0` to `max_m`
if symmetric is false, it also adds rbs and zbc modes

The 0,0 mode is not included and would need to be added separately
"""
function StellOpt.add_multiple_modes!(prob::StellOpt.Problem,
                                      wrapper::StellOpt.EquilibriumWrapper{T, E, D},
                                      min_n::Int,
                                      max_n::Int;
                                      max_m::Int = max_n,
                                      symmetric::Bool = true
                                     ) where {T, E <: Vmec, D <: StellOpt.AbstractDerivative}
    for n in min_n:max_n
        for m in 0:max_m
            #skip some unimportant mode
            if m == 0 && n <=0
              continue
            end
            s_parens = "("*string(n)*","*string(m)*")"
            StellOpt.add_variable!(prob, wrapper, "rbc"*s_parens)
            StellOpt.add_variable!(prob, wrapper, "zbs"*s_parens)
            if symmetric == false
              StellOpt.add_variable!(prob, wrapper, "rbs"*s_parens)
              StellOpt.add_variable!(prob, wrapper, "zbc"*s_parens)
            end  
        end
    end
    return nothing
end




function Vmec(wrapper::StellOpt.EquilibriumWrapper{T, E, D};
             ) where {T, E <: Vmec, D <: StellOpt.AbstractDerivative}
    return wrapper.eq
end

function VmecSurface(s,
                     wrapper::StellOpt.EquilibriumWrapper{T, E, D}
                    ) where {T, E <: Vmec, D <: StellOpt.AbstractDerivative}
    return VmecSurface(s, wrapper.eq)
end

function StellOpt.extract_derived_geometry(wrapper::StellOpt.EquilibriumWrapper{T, E, D},
                                           key::UInt;
                                          ) where {T, E <: Vmec, D <: StellOpt.AbstractDerivative}
    geom_call = Expr(wrapper.derived_geometries[key].head, 
    replace(wrapper.derived_geometries[key].args,
            :equilibrium => wrapper.eq)...)
    return eval(geom_call)
end

function StellOpt.update_input!(wrapper::StellOpt.EquilibriumWrapper{T, E, D};
                               ) where {T, E <: Vmec, D <: StellOpt.AbstractDerivative}
    vmec_data = VmecData(wrapper.eq)
    update_input_namelist!(wrapper.input, vmec_data)
    return nothing
end

function StellOpt.write_input_file(wrapper::StellOpt.EquilibriumWrapper{T, E, D},
                                   filename::AbstractString;
                                  ) where {T, E <: Vmec, D <: StellOpt.AbstractDerivative}
    writeVmecInput(wrapper.input, filename)
    return nothing
end
