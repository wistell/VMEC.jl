
function rotational_transform(vmec_surface::VmecSurface,
                              args...;
                              kwargs...
                             )
  return vmec_surface.iota[1]
end

function aspect_ratio(vmec_surface::VmecSurface,
                      args...;
                      kwargs...
                     )
  return vmec_surface.aspect
end

function magnetic_well(vmec_surface::VmecSurface,
                       args...;
                       kwargs...
                      )
  return -vmec_surface.vp[2]*vmec_surface.signgs
end

