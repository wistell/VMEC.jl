
struct CylindricalFromVmec <: Transformation; end
struct CartesianFromVmec <: Transformation; end
struct VmecFromFlux <: Transformation; end
struct FluxFromVmec <: Transformation; end
struct VmecFromPest <: Transformation; end
struct PestFromVmec <: Transformation; end
struct BoozerFromVmec <: Transformation; end
struct BoozerFromFlux <: Transformation; end
struct VmecFromBoozer <: Transformation; end
struct CylindricalFromBoozer <: Transformation; end
struct VmecFromCylindrical <: Transformation; end 
struct VmecFromCartesian <: Transformation; end 
#struct Derivatives end

#generic hooks
function (::PlasmaEquilibriumToolkit.InternalFromFlux)(x::FluxCoordinates, 
                                                       vmecsurf::VmecSurface;
                                                      )
  return VmecFromFlux()(x, vmecsurf)
end


function (::PlasmaEquilibriumToolkit.InternalFromPest)(x::PestCoordinates, 
                                                       vmecsurf::VmecSurface;
                                                      )
  return VmecFromPest()(x, vmecsurf)
end

function (::PlasmaEquilibriumToolkit.InternalFromCylindrical)(x::Cylindrical, 
                                                              vmec::Vmec;
                                                             )
  return VmecFromCylindrical()(x, vmec)
end

function (::PlasmaEquilibriumToolkit.InternalFromCartesian)(x::SVector, 
                                                            vmec::Vmec;
                                                           )
  return VmecFromCartesian()(x, vmec)
end

function (::PlasmaEquilibriumToolkit.FluxFromInternal)(x::VmecCoordinates,
                                                       vmecsurf::VmecSurface;
                                                      )
  return FluxFromVmec()(x, vmecsurf)
end

function (::PlasmaEquilibriumToolkit.PestFromInternal)(x::VmecCoordinates,
                                                       vmecsurf::VmecSurface;
                                                      )
  return PestFromVmec()(x, vmecsurf)
end


function (::VmecFromFlux)(x::FluxCoordinates,
                          vmecsurf::VmecSurface;
                         )
  y = FluxCoordinates(x.ψ,x.θ,-x.ζ)
  #s = x.ψ*2π/vmecsurf.phi[end]*vmecsurf.signgs
  s = vmecsurf.s
  θ = θ_internal(y,vmecsurf)
  ζ = -x.ζ
  return VmecCoordinates(s,θ,ζ)
end

#Keep this function for legacy reasons
function (::CylindricalFromVmec)(x::VmecCoordinates,
                                 vmecsurf::VmecSurface;
                                )
  return PlasmaEquilibriumToolkit.CylindricalFromInternal()(x, vmecsurf)
end

function (::CylindricalFromVmec)(x::VmecCoordinates,
                                vmec::Vmec;
                               )
  s = x.s
  rmn = VMEC.VmecFourierDataArray(s, vmec, :rmn)
  zmn = VMEC.VmecFourierDataArray(s, vmec, :zmn)
  r = inverseTransform(x, rmn)
  z = inverseTransform(x, zmn)
  return Cylindrical(r, x.ζ, z)
end


function (::CylindricalFromFlux)(x::FluxCoordinates,
                                 vmecsurf::VmecSurface;
                                )
  v = VmecFromFlux()(x, vmecsurf)
  return CylindricalFromVmec()(v, vmecsurf)
end

#=
function (::CylindricalFromVmec)(x::VmecCoordinates,
                                 vmecsurf::VmecSurface,
                                 ::Derivatives;
                                )
  T = typeof(x.θ)
  d = derivatives(x, vmecsurf)
  return CylindricalFromVmec()(x,vmecsurf), @SMatrix [d[1,1] d[1,2] d[1,3];
                                                  zero(T) zero(T) one(T);
                                                  d[3,1] d[3,2] d[3,3]]
end


function (::CartesianFromVmec)(x::VmecCoordinates,
                               vmecsurf::VmecSurface;
                              )
  return CartesianFromInternal()(c)
end
=#

function (::CartesianFromVmec)(x::VmecCoordinates,
                               vmec::VmecSurface;
                              )
  cc = CylindricalFromVmec()(x, vmec)
  return CartesianFromCylindrical()(cc)
end
  

function (::CartesianFromFlux)(x::FluxCoordinates,
                               vmecsurf::VmecSurface;
                              )
  v = VmecFromFlux()(x, vmecsurf)
  return CartesianFromVmec()(v, vmecsurf)
end

function (::CartesianFromPest)(x::PestCoordinates,
                               vmecsurf::VmecSurface;
                              )
    v = VmecFromPest()(x, vmecsurf)
    return CartesianFromVmec()(v, vmecsurf)
end

function (::FluxFromPest)(x::PestCoordinates,
                          vmecsurf::VmecSurface;
                         )
  # VMEC is a left handed coordinate system and a positive rotational transform
  # implies the theta coordinate increases with positive toroidal angle (CCW direction).
  # FluxCoordinates and PestCoordinates are right handed coordinate systems, so to match
  # that ∇ψ > 0 and ∇θ > 0 at (θ,ζ) = (0,0), increasing toroidal angle is negative of the
  # cylindrical toroidal angle and increases CW.  This means the rotational transform
  # in these coordinates is the negative of the rotational transform in VMEC
  return FluxCoordinates(x.ψ,x.α-vmecsurf.iota[1]*x.ζ,x.ζ)
end

function (::VmecFromPest)(x::PestCoordinates,
                          vmecsurf::VmecSurface;
                         )
  y = FluxFromPest()(x, vmecsurf)
  return VmecFromFlux()(y, vmecsurf)
end

function (::FluxFromVmec)(x::VmecCoordinates,
                        vmecsurf::VmecSurface;
                       )
  ψ = vmecsurf.phi[1]/(2π)*vmecsurf.signgs
  θ = x.θ + inverseTransform(x, vmecsurf.λmn)
  ζ = -x.ζ
  return FluxCoordinates(ψ, θ, ζ)
end

function (::PestFromVmec)(x::VmecCoordinates,
                          vmecsurf::VmecSurface;
                         )
  y = FluxFromVmec()(x, vmecsurf)
  α = y.θ + vmecsurf.iota[1] * y.ζ
  return PestCoordinates(y.ψ, α, y.ζ)
end

function (::VmecFromCylindrical)(cc::Cylindrical{T, T},
                                 vmec::Vmec) where {T}
  #calculate guess for theta
  sg, θg = PlasmaEquilibriumToolkit.flux_surface_and_angle(cc, vmec)
  #construct bounds arrays
  lower = [0.0, θg - π/2]
  upper = [1.0, θg + π/2]
  guess = [sg, θg]
  ζ = cc.θ

  #optimized function
  function f(x)
    s = x[1]
    θ = x[2]
    vc = VmecCoordinates(s, θ, ζ)
    ccg = CylindricalFromVmec()(vc, vmec)
    return (ccg.r - cc.r)^2 + (ccg.z - cc.z)^2
  end

  res = optimize(f, lower, upper, guess)
  s = res.minimizer[1]
  θ = mod2pi(res.minimizer[2])
  return VmecCoordinates(s, θ, ζ)
end

function (::VmecFromCartesian)(xyz::SVector{3, T}, 
                               vmec::Vmec{T}
                              ) where {T}
  cc = CylindricalFromCartesian()(xyz)
  return VmecFromCylindrical()(cc, vmec)
end

function PlasmaEquilibriumToolkit.flux_surface_and_angle(x::Cylindrical{T,T}, 
                   vmec::Vmec{FT}) where {T, FT <: AbstractFloat}
  ζ = x.θ
  rmn_boundary = VMEC.VmecFourierDataArray(1.0, vmec, :rmn)
  zmn_boundary = VMEC.VmecFourierDataArray(1.0, vmec, :zmn)
  rmn_core = VMEC.VmecFourierDataArray(0.0, vmec, :rmn)
  zmn_core = VMEC.VmecFourierDataArray(0.0, vmec, :zmn)
  vc = VmecCoordinates(0.5, 0.0, ζ)
  r_boundary = inverseTransform(vc, rmn_boundary)
  z_boundary = inverseTransform(vc, zmn_boundary)
  r_core = inverseTransform(vc, rmn_core)
  z_core = inverseTransform(vc, zmn_core)

  #line from core to edge
  r_plasma = r_boundary - r_core
  z_plasma = z_boundary - z_core

  #line from core to desired point
  r_point = x.r - r_core
  z_point = x.z - z_core

  #calculate the thetas
  θ_plasma = atan(z_plasma, r_plasma)
  θ_point = atan(z_point, r_point)

  θguess = θ_point - θ_plasma

  #get new boundary points
  vc = VmecCoordinates(0.5, θguess, ζ)
  r_boundary = inverseTransform(vc, rmn_boundary)
  z_boundary = inverseTransform(vc, zmn_boundary)

  #we now calculate r/a^2 = s for out point
  a = (r_boundary - r_core)^2 + (z_boundary - z_core)^2
  r = (x.r - r_core)^2 + (x.z - z_core)^2
  sguess = r/a

  if sguess >= 1.0
    sguess = 0.99
  elseif sguess <= 0.0
    sguess = 0.01
  end

  return sguess, θguess
  
end


function PlasmaEquilibriumToolkit.basis_vectors(::Covariant,
                                                ::CartesianFromVmec,
                                                x::VmecCoordinates,
                                                vmecsurf::VmecSurface
                                               )
  c = CylindricalFromVmec()(x,vmecsurf)
  d = PlasmaEquilibriumToolkit.transform_deriv(CylindricalFromInternal(),x,vmecsurf)
  sζ, cζ = sincos(x.ζ)
  dXds_x = d[1,1]*cζ
  dXds_y = d[1,1]*sζ
  dXds_z = d[3,1]

  dXdθ_x = d[1,2]*cζ
  dXdθ_y = d[1,2]*sζ
  dXdθ_z = d[3,2]

  dXdζ_x = d[1,3]*cζ - c.r*sζ
  dXdζ_y = d[1,3]*sζ + c.r*cζ
  dXdζ_z = d[3,3]

  return @SMatrix [dXds_x dXdθ_x dXdζ_x;
                   dXds_y dXdθ_y dXdζ_y;
                   dXds_z dXdθ_z dXdζ_z]
end

function PlasmaEquilibriumToolkit.basis_vectors(::Covariant,
                                                ::CartesianFromInternal,
                                                x::VmecCoordinates,
                                                vmecsurf::VmecSurface;
                                               )
    return basis_vectors(Covariant(), CartesianFromVmec(), x, vmecsurf)
end

function PlasmaEquilibriumToolkit.basis_vectors(::Contravariant,
                                                ::CartesianFromVmec,
                                                x::VmecCoordinates,
                                                vmecsurf::VmecSurface;
                                               )
  g = inverseTransform(x,vmecsurf.gmn)
  return transform_basis(ContravariantFromCovariant(),basis_vectors(Covariant(),CartesianFromVmec(),x,vmecsurf),g)
end

function PlasmaEquilibriumToolkit.basis_vectors(::Contravariant,
                                                ::CartesianFromInternal,
                                                x::VmecCoordinates,
                                                vmecsurf::VmecSurface;
                                               )
    return basis_vectors(Contravariant(), CartesianFromVmec(), x, vmecsurf)
end

function PlasmaEquilibriumToolkit.basis_vectors(::Covariant,
                                                ::CartesianFromPest,
                                                x::PestCoordinates,
                                                vmecsurf::VmecSurface;
                                               )
  contravariantPestBasis = basis_vectors(Contravariant(),CartesianFromPest(),x,vmecsurf)
  return transform_basis(CovariantFromContravariant(),contravariantPestBasis)
end

function PlasmaEquilibriumToolkit.basis_vectors(::Contravariant,
                                                ::CartesianFromPest,
                                                x::PestCoordinates,
                                                vmecsurf::VmecSurface;
                                               )
  v = VmecFromPest()(x,vmecsurf)
  contravariantVmecBasis = basis_vectors(Contravariant(),CartesianFromVmec(),v,vmecsurf)
  return transform_basis(PestFromVmec(),v,contravariantVmecBasis,vmecsurf)
end

function PlasmaEquilibriumToolkit.basis_vectors(::Covariant,
                                                ::CartesianFromFlux,
                                                x::FluxCoordinates,
                                                vmecsurf::VmecSurface;
                                               )
  contravariantFluxBasis = basis_vectors(Contravariant(),CartesianFromFlux(),x,vmecsurf)
  return transform_basis(CovariantFromContravariant(),contravariantFluxBasis)
end

function PlasmaEquilibriumToolkit.basis_vectors(::Contravariant,
                                                ::CartesianFromFlux,
                                                x::FluxCoordinates,
                                                vmecsurf::VmecSurface;
                                               )
  v = VmecFromFlux()(x,vmecsurf)
  contravariantVmecBasis = basis_vectors(Contravariant(),CartesianFromVmec(),v,vmecsurf)
  return transform_basis(FluxFromVmec(),v,contravariantVmecBasis,vmecsurf)
end

# The transform_basis functions need to take into account the definitions of rotational
# transform in the respective coordinate systems
# αₚ = θₚ - ιₚζₚ = θᵥ + λ(s,θᵥ,ϕᵥ) - (-ιᵥ)(-ϕᵥ)
# => ∇αₚ = ∇θᵥ + dλ/ds ∇s + dλ/dθᵥ ∇θᵥ + dλ/dϕᵥ ∇ϕᵥ - dιᵥ/ds ϕᵥ ∇s - ιᵥ ∇ϕᵥ
# => ∇αₚ = (dλ/ds - dιᵥ/ds ϕᵥ)∇s+ (1 + dλ/dθᵥ)∇θᵥ + (dλ/dϕᵥ - ιᵥ)∇ϕᵥ
function PlasmaEquilibriumToolkit.transform_basis(::PestFromVmec,
                                                  v::VmecCoordinates,
                                                  e::BasisVectors,
                                                  vmecsurf::VmecSurface;
                                                 )
  d = PlasmaEquilibriumToolkit.derivatives(v,vmecsurf,:λ)
  Φ = vmecsurf.phi[end]/(2π)*vmecsurf.signgs
  ∇ψ = Φ*e[:,1]
  ∇ζ = -e[:,3]
  ∇α = sign(Φ)*(e[:,1]*(d[1] - vmecsurf.iota[2]*v.ζ)
                + e[:,2]*(d[2] + 1) +
                e[:,3]*(d[3] - vmecsurf.iota[1]))
  return hcat(∇ψ, ∇α, ∇ζ)
end

function PlasmaEquilibriumToolkit.transform_basis(::PestFromInternal,
                                                  v::VmecCoordinates,
                                                  e::BasisVectors,
                                                  vmecsurf::VmecSurface;
                                                 )
    return PET.transform_basis(PestFromVmec(), v, e, vmecsurf)
end

# => ∇θ = ∇θᵥ + dλ/ds ∇s + dλ/dθᵥ ∇θᵥ + dλ/dϕᵥ ∇ϕᵥ
function PlasmaEquilibriumToolkit.transform_basis(::FluxFromVmec,
                                                  v::VmecCoordinates,
                                                  e::BasisVectors,
                                                  vmecsurf::VmecSurface;
                                                 )
  d = PlasmaEquilibriumToolkit.derivatives(v,vmecsurf,:λ)
  Φ = vmecsurf.phi[end]/(2π)*vmecsurf.signgs
  ∇ψ = Φ*e[:,1]
  ∇ζ = -e[:,3]
  ∇θ = sign(Φ)*(e[:,1]*d[1] + e[:,2]*(1 + d[2]) + e[:,3]*d[3])
  return hcat(∇ψ, ∇θ, ∇ζ)
end

function PlasmaEquilibriumToolkit.transform_basis(::FluxFromInternal,
                                                  v::VmecCoordinates,
                                                  e::BasisVectors,
                                                  vmecsurf::VmecSurface;
                                                 )
    return PET.transform_basis(FluxFromVmec(), v, e, vmecsurf)
end

# Compute the normalized global magnetic shear
function shat(vmecsurf::VmecSurface)
  return -2*vmecsurf.s*vmecsurf.iota[2]/vmecsurf.iota[1]
end
