"""
    BoozerFromVmec()(x::VmecCoordinates, vmecsurf::VmecSurface)
    BoozerFromVmec()(x::AbstractArray{VmecCoordinates}, vmecsurf::VmecSurface)
    BoozerFromVmec()(x::VmecCoordinates, vmecsurf::VmecSurface, wmn::Array{SurfaceFourierData}, boozerG, boozerI)
    BoozerFromVmec()(x::AbstractArray{VmecCoordinates}, vmecsurf::VmecSurface, wmn::Array{SurfaceFourierData}, boozerG, boozerI)

Compute the transformation from flux coordinates to Boozer coordinates using
data from `vmec`, a `VmecSurface` object.  The terms `wmn`, `boozerG` and
`boozerI` need only be computed once per `VmecSurface`.

See also [`periodic_boozer_function`](@ref), [`SurfaceFourierData`](@ref)
"""
function (::BoozerFromVmec)(x::VmecCoordinates,
                            vmecsurf::VmecSurface{T},
                            wmn::AbstractArray{SurfaceFourierData{T}},
                            boozerG::T,
                            boozerI::T;
                           ) where {T <: AbstractFloat}
  w = inverseTransform(x,wmn)
  λ = inverseTransform(x,vmecsurf.λmn)
  jacFac = 1.0/(boozerG + vmecsurf.iota[1]*boozerI)
  ν = (w - boozerI*λ)*jacFac
  ψ = vmecsurf.phi[1]
  χ = x.θ + λ + vmecsurf.iota[1]*ν
  ϕ = x.ζ + ν
  return BoozerCoordinates(ψ,χ,ϕ)
end

function (::BoozerFromVmec)(x::VmecCoordinates,
                            vmecsurf::VmecSurface{T};
                           ) where {T <: AbstractFloat}
  wmn, boozerG, boozerI = periodic_boozer_function(vmecsurf)
  return BoozerFromVmec()(x, vmecsurf, wmn, boozerG, boozerI)
end

function (::BoozerFromVmec)(x::AbstractArray{FluxCoordinates},
                            vmecsurf::VmecSurface{T};
                           ) where {T <: AbstractFloat}
  ψ = vmecsurf.phi
  all(p -> p.ψ == ψ, x) || "Flux surface labels must be identical for each point"
  wmn, boozerG, boozerI = periodic_boozer_function(vmecsurf)
  return BoozerFromVmec()(x, vmec, wmn, boozerG, boozerI)
end

function (::BoozerFromVmec)(x::StructArray{VmecCoordinates{T, T}, N, C, I},
                            vmecsurf::VmecSurface{T},
                            wmn::AbstractArray{SurfaceFourierData{T}},
                            boozerG::T,
                            boozerI::T;
                           ) where {T <: AbstractFloat, N, C, I}
  res = StructArray{BoozerCoordinates{T,T}}(undef, size(x))
  @batch minbatch=16 for i in eachindex(x, res)
    res[i] = BoozerFromVmec()(x[i], vmecsurf, wmn, boozerG, boozerI)
  end
  return res
end

"""
    VmecFromBoozer()(bc::BoozerCoordinates, vmecsurf::VmecSurface); mBoozer, nBoozer)
    VmecFromBoozer()(bc::StructArray{BoozerCoordinates}, vmecsurf::VmecSurface; mBoozer, nBoozer)
    VmecFromBoozer()(bc::BoozerCoordinates, boozsurf::BoozerSurface)
    VmecFromBoozer()(bc::StructArray{BoozerCoordinates}, boozsurf::BoozerSurface)
"""
function (::VmecFromBoozer)(bc::BoozerCoordinates,
                            vmecsurf::VmecSurface{T};
                           mBoozer = 48, nBoozer = 15) where T
  boozer_ν = boozer_fourier_spectrum(:νmn, vmecsurf, mBoozer, nBoozer)
  boozer_λ = boozer_fourier_spectrum(:λmn, vmecsurf, mBoozer, nBoozer)
  λ = inverseTransform(bc, boozer_λ)
  ν = inverseTransform(bc, boozer_ν)
  s = vmecsurf.s
  θ = bc.χ - vmecsurf.iota[1]*ν - λ
  ζ = bc.ϕ - ν
  return VmecCoordinates(s, θ, ζ)

end

function (::VmecFromBoozer)(x::StructArray{VmecCoordinates{T, T}, N, C, I},
                            vmecsurf::VmecSurface{T};
                            mBoozer = 48, nBoozer = 15) where {T <: AbstractFloat, N, C, I}
  res = StructArray{VmecCoordinates{T,T}}(undef, size(x))
  @batch minbatch=16 for i in eachindex(x, res)
    res[i] = VmecFromBoozer()(x[i], vmecsurf, mBoozer=mBoozer, nBoozer=nBoozer)
  end
  return res
end

function (::VmecFromBoozer)(bc::BoozerCoordinates, boozsurf::BoozerSurface{T}) where {T}
  ν = inverseTransform(bc, boozsurf.νmn)
  λ = inverseTransform(bc, boozsurf.λmn)
  s = boozsurf.s
  θ = bc.χ - boozsurf.iota[1]*ν - λ
  ζ = bc.ϕ - ν
  return VmecCoordinates(s, θ, ζ)
end

function (::VmecFromBoozer)(x::StructArray{BoozerCoordinates{T, T}, N, C, I},
                            boozsurf::BoozerSurface{T};
                            ) where {T <: AbstractFloat, N, C, I}
  res = StructArray{VmecCoordinates{T,T}}(undef, size(x))
  @batch minbatch=16 for i in eachindex(x, res)
    res[i] = VmecFromBoozer()(x[i], boozsurf)
  end
  return res
end
"""
    CylindricalFromBoozer()(bc::BoozerCoordinates, vmecsurf::VmecSurface); mBoozer, nBoozer)
    CylindricalFromBoozer()(bc::StructArray{BoozerCoordinates}, vmecsurf::VmecSurface; mBoozer, nBoozer)
    CylindricalFromBoozer()(bc::BoozerCoordinates, boozsurf::BoozerSurface)
    CylindricalFromBoozer()(bc::StructArray{BoozerCoordinates}, boozsurf::BoozerSurface)
"""
 
function (::CylindricalFromBoozer)(bc::BoozerCoordinates, vmecsurf::VmecSurface{T};
                                  mBoozer=48, nBoozer=15) where {T <: AbstractFloat}
  zmn = boozer_fourier_spectrum(:zmn, vmecsurf, mBoozer, nBoozer)
  rmn = boozer_fourier_spectrum(:rmn, vmecsurf, mBoozer, nBoozer)
  νmn = boozer_fourier_spectrum(:νmn, vmecsurf, mBoozer, nBoozer)
  r = inverseTransform(bc, rmn)
  z = inverseTransform(bc, zmn)
  θ = bc.ϕ -inverseTransform(bc, νmn) 
  
  return Cylindrical(r, θ, z)
end

function (::CylindricalFromBoozer)(x::StructArray{BoozerCoordinates{T, T}, N, C, I},
                            vmecsurf::VmecSurface{T}; mBoozer=48, nBoozer=15
                            ) where {T <: AbstractFloat, N, C, I}
  res = StructArray{Cylindrical{T,T}}(undef, size(x))
  @batch minbatch=16 for i in eachindex(x, res)
    res[i] = CylindricalFromBoozer()(x[i], vmecsurf, mBoozer=mBoozer, nBoozer=nBoozer)
  end
  return res
end

function (::CylindricalFromBoozer)(bc::BoozerCoordinates{T, T}, boozsurf::BoozerSurface{T}) where {T}
  z = inverseTransform(bc, boozsurf.zmn)
  r = inverseTransform(bc, boozsurf.rmn)
  ν = inverseTransform(bc, boozsurf.νmn)
  θ = bc.ϕ - ν

  return Cylindrical(r, θ, z)
end

function (::CylindricalFromBoozer)(x::StructArray{BoozerCoordinates{T, T}, N, C, I},
                            boozsurf::BoozerSurface{T}; 
                            ) where {T <: AbstractFloat, N, C, I}
  res = StructArray{Cylindrical{T,T}}(undef, size(x))
  @batch minbatch=16 for i in eachindex(x, res)
    res[i] = CylindricalFromBoozer()(x[i], boozsurf)
  end
  return res
end
"""
  boozer_ν(vmecsurf::VmecSurface)

calculates the "boozer_ν" function in vmec coordinates. 
In Hirschman's notes this is given as "p" but in order to avoid confusion
with pressure, we adopt Matt Landreman's convention and use ν
"""
function boozer_ν(vmecsurf::VmecSurface{T}) where T
  wmn, boozerG, boozerI = periodic_boozer_function(vmecsurf)
  jacFac = 1.0/(boozerG + vmecsurf.iota[1]*boozerI)
  νmn = Vector{SurfaceFourierData{T}}(undef, vmecsurf.mnmax_nyq)
  λmn = vmecsurf.λmn
  λm = map(i->i.m, λmn)
  λn = map(i->i.n, λmn)
  for i in 1:vmecsurf.mnmax_nyq
    m = Float64(wmn[i].m)
    n = Float64(wmn[i].n)
    index = findfirst((λm.==m).*(λn.==n).==1)
    if index == nothing
      νmn_cos = (wmn[i].cos) * jacFac
      νmn_sin = (wmn[i].sin) * jacFac
    else
      νmn_cos = (wmn[i].cos - boozerI * λmn[index].cos) * jacFac
      νmn_sin = (wmn[i].sin - boozerI * λmn[index].sin) * jacFac
    end
    νmn[i] = SurfaceFourierData(m, n, νmn_cos, νmn_sin, 0.0, 0.0, 0.0, 0.0)
  end
  return νmn
end

"""
    boozer_jacobian(::BoozerFromVmec, v::FluxCoordinates, vmec::VmecSurface)
    boozer_jacobian(::BoozerFromVmec, v::AbstractArray{FluxCoordinates}, vmec::VmecSurface)
    boozer_jacobian(::BoozerFromVmec, v::FluxCoordinates, vmec::VmecSurface, wmn::Array{SurfaceFourierData}, boozerG, boozerI)
    boozer_jacobian(::BoozerFromVmec, v::AbstractArray{FluxCoordinates}, vmec::VmecSurface, wmn::Array{SurfaceFourierData}, boozerG, boozerI)

Compute the Jacobian of the coordinate transformation to Boozer coordinates from flux coordinates.

See also: [`periodic_boozer_function`](@ref)
"""
function boozer_jacobian(::BoozerFromVmec,
                         v::VmecCoordinates,
                         vmec::VmecSurface{T},
                         wmn::AbstractVector{SurfaceFourierData{T}},
                         boozerG::T,
                         boozerI::T;
                        ) where {T <: AbstractFloat}
  jacFac = 1.0/(boozerG + vmec.iota[1]*boozerI)
  dλdθ = inverseTransform(v,vmec.λmn;deriv=:dθ)
  dλdζ = inverseTransform(v,vmec.λmn;deriv=:dζ)
  dpdθ = (inverseTransform(v,wmn;deriv=:dθ) - boozerI*dλdθ)*jacFac
  dpdζ = (inverseTransform(v,wmn;deriv=:dζ) - boozerI*dλdζ)*jacFac
  return (1+dλdθ)*(1+dpdζ) + (vmec.iota[1] - dλdζ)*dpdθ
end

function boozer_jacobian(::BoozerFromVmec,
                         v::VmecCoordinates,
                         vmec::VmecSurface{T};
                        ) where {T <: AbstractFloat}
  wmn, boozerG, boozerI = periodic_boozer_function(vmec)
  return boozer_jacobian(BoozerFromVmec(), v, vmec, wmn, boozerG, boozerI)
end

function boozer_jacobian(::BoozerFromVmec,
                         v::AbstractArray{VmecCoordinates},
                         vmec::VmecSurface{T};
                        ) where {T <: AbstractFloat}
  ψ = first(v).ψ
  all(p -> p.ψ == ψ, v) || "Flux surface labels must be identical for each point"
  wmn, boozerG, boozerI = periodic_boozer_function(vmec)
  return boozer_jacobian(BoozerFromVmec(), v, vmec, wmn, boozerG, boozerI)
end

function boozer_jacobian(::BoozerFromVmec,
                         v::StructArray{VmecCoordinates{T, T}, N, C, I},
                         vmec::VmecSurface{T},
                         wmn::AbstractArray{SurfaceFourierData{T}},
                         boozerG::T,
                         boozerI::T;
                        ) where {T <: AbstractFloat, N, C, I}
  res = Array{T}(undef, size(v))
  @batch minbatch=16 for i in eachindex(v, res)
    res[i] = boozer_jacobian(BoozerFromVmec(), v[i], vmec, wmn, boozerG, boozerI)
  end
  return res
end

@doc raw"""
    periodic_boozer_function(vmec::VmecSurface)

Compute the auxillary Boozer transformation data `wmn`, `boozerG`, and `boozerI`,
where `wmn` is the `SurfaceFourierData` spectrum ``\frac{B_\theta}{m} - \frac{B_\phi}{n}``
and `boozerG` is the enclosed toroidal current and `boozerI` is the enclosed poloidal current.

See also: [`BoozerFromVmec`](@ref)
"""
function periodic_boozer_function(vmec::VmecSurface{T};
                                 ) where {T <: AbstractFloat}
  boozerG = vmec.bsubvmn[1].cos
  boozerI = vmec.bsubumn[1].cos
  zeros_T = zeros(T,vmec.mnmax_nyq)
  wmn_cos = Vector{T}(undef,vmec.mnmax_nyq)
  wmn_sin = similar(wmn_cos)
  for mn in eachindex(wmn_cos,wmn_sin)
    m = vmec.xm_nyq[mn]
    n = vmec.xn_nyq[mn]
    if m != 0 && n != 0
      wmn_sin[mn] = 0.5*(vmec.bsubumn[mn].cos/m - vmec.bsubvmn[mn].cos/n)
      wmn_cos[mn] = 0.5*(vmec.bsubumn[mn].sin/m - vmec.bsubvmn[mn].sin/n)
    elseif m != 0
      wmn_sin[mn] = vmec.bsubumn[mn].cos/m
      wmn_cos[mn] = vmec.bsubumn[mn].sin/m
    elseif n != 0
      wmn_sin[mn] = -vmec.bsubvmn[mn].cos/n
      wmn_cos[mn] = -vmec.bsubvmn[mn].sin/n
    else
      wmn_sin[mn] = zero(T)
      wmn_cos[mn] = zero(T)
    end
  end
  return StructArray{SurfaceFourierData{T}}((vmec.xm_nyq,vmec.xn_nyq,wmn_cos,wmn_sin,zeros_T,zeros_T, zeros_T, zeros_T)), boozerG, boozerI
end

@doc raw"""
    boozer_grid(vmec::VmecSurface, mBoozer, nBoozer)

Construct a grid of flux coordinates and associated Boozer coordinates
of size (2(2`mBoozer`+1) × 2(2`nBoozer`)+1)).
"""
function boozer_grid(vmec::VmecSurface{T},
                     mBoozer::Int,
                     nBoozer::Int;
                    ) where {T <: AbstractFloat}
  θ_range = range(0.0,step=2π/(2*(2*mBoozer+1)),length=2*(2*mBoozer+1))
  ζ_range = range(0.0,step=2π/(vmec.nfp*2*(2*nBoozer+1)),length=2(2*nBoozer+1))
  u = MagneticCoordinateGrid(VmecCoordinates,vmec.s,θ_range,ζ_range)
  wmn, boozerG, boozerI = periodic_boozer_function(vmec)
  b = BoozerFromVmec()(u, vmec, wmn, boozerG, boozerI)
  jac = boozer_jacobian(BoozerFromVmec(), u, vmec, wmn, boozerG, boozerI)
  return u, b, jac
end

@doc raw"""
    boozer_mode_indices(mBoozer, nBoozer, nfp)

Construct the list of unique Boozer mode indices for a given `mBoozer`, `nBoozer` and `nfp`.
"""
function boozer_mode_indices(mBoozer::Int,
                             nBoozer::Int,
                             nfp::Int;
                            )
  mnBoozer = nBoozer + 1 + (mBoozer-1)*(1 + 2*nBoozer)
  modes = Vector{SVector{2,Int}}(undef,mnBoozer)
  i = 1
  for n = 0:nfp:nfp*nBoozer
    modes[i] = SVector{2,Int}(0,n)
    i += 1
  end
  for m = 1:mBoozer-1
    for n = -nfp*nBoozer:nfp:nfp*nBoozer
      modes[i] = SVector{2,Int}(m,n)
      i += 1
    end
  end
  return modes, mnBoozer
end


function calculate_boozer_spectrum(field::Symbol,
                                   vmecsurf::VmecSurface{T},
                                   mnBoozer::Int,
                                   modes::Vector{SVector{2, Int}},
                                   scale_factor::T,
                                   u::StructArray{VmecCoordinates{T,T}},
                                   b::StructArray{BoozerCoordinates{T,T}},
                                   jac::Matrix{T};) where {T}
                                  



  m_vec = Vector{T}(undef,mnBoozer)
  n_vec = similar(m_vec)
  cos_vec = similar(m_vec)
  sin_vec = similar(m_vec)
  dcos_vec = similar(m_vec)
  dsin_vec = similar(m_vec)
  data = Matrix{T}(undef, size(u))
  if field == :νmn
    vmec_field = boozer_ν(vmecsurf)
  else
    vmec_field = getfield(vmecsurf, field)
  end

  for i in eachindex(u, jac, data)
    data[i] = inverseTransform(u[i], vmec_field) * jac[i]
  end
  @batch minbatch=128 for i in eachindex(modes)
    #The cosine multiplication is needed to match xbooz results
    #It's not clear whether the sine value at m=0, n=0 is needed. This value is always 0 for
    #the configurations I have
    if modes[i][1] == 0 && modes[i][2] == 0
      scale_val = scale_factor * 0.5
    else
      scale_val = scale_factor
    end
    
    m_vec[i] = modes[i][1]
    n_vec[i] = modes[i][2]
    cos_vec[i] = cosineTransform(modes[i][1], modes[i][2], b, data)*scale_val
    sin_vec[i] = sineTransform(modes[i][1], modes[i][2], b, data)*scale_val
    #todo: eventually calculate the derivatives
    dcos_vec[i] = 0.0
    dsin_vec[i] = 0.0
  end
  zeros_T = zeros(T,length(m_vec))
  return StructArray{SurfaceFourierData{T}}((m_vec,n_vec,cos_vec,sin_vec,
                                          dcos_vec,dsin_vec,
                                          zeros_T, zeros_T))

end



@doc raw"""
    boozer_fourier_spectrum(field::Symbol, vmec::VmecSurface, mBoozer, nBoozer)
    boozer_fourier_spectrum(field::Symbol, surfaces, vmec::Vmec, mBoozer, nBoozer)

Compute the Boozer mode amplitudes for a VMEC field `field`
on a `VmecSurface` or list of surfaces from a VMEC equilibrium
defined by a Fourier series representation using `mBoozer` × `nBoozer` modes.
"""
function boozer_fourier_spectrum(field::Symbol,
                                vmecsurf::VmecSurface{T},
                                mBoozer::Int,
                                nBoozer::Int;
                               ) where {T}
  modes, mnBoozer = boozer_mode_indices(mBoozer, nBoozer, vmecsurf.nfp)
  scale_factor = 2.0/(2 * (2 * mBoozer + 1) * 2 * (2 * nBoozer + 1))
  #Set up the coordinates, field data and Jacobian of the transformation
  u, b, jac = boozer_grid(vmecsurf, mBoozer, nBoozer)

  if field == :νmn
    vmec_field = boozer_ν(vmecsurf)
  else
    vmec_field = getfield(vmecsurf, field)
  end

  return calculate_boozer_spectrum(field, vmecsurf, mnBoozer, modes, 
                                   scale_factor, u, b, jac)
end

function boozer_fourier_spectrum(field::Symbol,
                                surfaces::AbstractVector{T},
                                vmec::Vmec{T},
                                mBoozer::Int,
                                nBoozer::Int;
                               ) where {T <: AbstractFloat}
  spectrumData = Vector{Vector{SurfaceFourierData}}(undef, length(surfaces))
  for i in eachindex(surfaces, spectrumData)
    vmecSurface = VmecSurface(surfaces[i], vmec)
    spectrumData[i] = boozer_fourier_spectrum(field, vmecSurface, mBoozer, nBoozer)
  end
  return spectrumData
end

@doc raw"""
    quasisymmetry_deviation(mQS, nQS, mBoozer, nBoozer, vmec::VmecSurface)
    quasisymmetry_deviation(mQS, nQS, mBoozer, nBoozer, vmecSurfaces::Vector{VmecSurface})

Compute the quasisymmetry deviation measure of a `VmecSurface`:

``QS_{deviation} = \frac{\sum\limits_{m', n'} B_{m',n'}^2}{B_{0,0}^2}, \text{with } m' \neq \alpha m_{QS},\,n' \neq \alpha n_{QS}``

where ``\alpha \in \mathbb{Z}``.  If a vector of `VmecSurface` objects are supplied,
the quasisymmetry deviation will be computed at each surface.
"""
function quasisymmetry_deviation(mQS::Int,
                                 nQS::Int,
                                 mBoozer::Int,
                                 nBoozer::Int,
                                 vmecsurf::VmecSurface{T};
                                ) where {T <: AbstractFloat}
  modB = boozer_fourier_spectrum(:bmn, vmecsurf, mBoozer, nBoozer)
  polModes = map(i->Int(i.m), modB)
  torModes = map(i->Int(i.n), modB)
  denom = abs(modB[1].cos)
  asymPower = sum(map(i->i.cos^2, modB[2:end]))
  m = mQS
  n = nQS
  while m <= (mBoozer-1) && abs(n) <= abs(vmecsurf.nfp*nBoozer)
    index = findfirst((polModes.==m).*(torModes.==n).==1)
    asymPower -= modB[index].cos^2 #todo: include sine modes for asym?
    m += mQS
    n += nQS
  end
  return asymPower > zero(typeof(asymPower)) ? sqrt(asymPower)/denom : zero(typeof(asymPower))
end

function quasisymmetry_deviation(mQS::Int,
                                 nQS::Int,
                                 mBoozer::Int,
                                 nBoozer::Int,
                                 vmecSurfaces::AbstractVector{VmecSurface{T}};
                                ) where {T}
  qsValues = Vector{T}(undef,length(vmecSurfaces))
  for i in eachindex(qsValues,vmecSurfaces)
    qsValues[i] = quasisymmetry_deviation(mQS,nQS,mBoozer,nBoozer,vmecSurfaces[i])
  end
  return qsValues
end
