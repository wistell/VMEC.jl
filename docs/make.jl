using VMEC
using Documenter

makedocs(;
    modules = [VMEC],
    authors = "Benjamin Faber <bfaber@wisc.edu> and contributors",
    repo = "https://gitlab.com/wistell/VMEC.jl/blob/{commit}{path}#L{line}",
    sitename = "VMEC.jl",
    format = Documenter.HTML(;
        prettyurls = get(ENV, "CI", "false") == "true",
        canonical = "https://wistell.gitlab.io/VMEC.jl",
        assets = String[],
    ),
    pages = [
        "VMEC.jl" => "index.md",
        "Running VMEC from Julia" => "running_vmec.md",
        "Loading VMEC output" => "reading_vmec.md",
        "Working with VMEC data" => "working_with_data.md",
        "Library" => [
                      "VMEC_jll" => "library/VMEC_jll.md",
                      "API Reference" => [
                                          "Public" => "library/public.md",
                                          "Private" => "library/private.md",
                                         ],
                     ],
    ],
)
