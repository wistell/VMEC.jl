```@meta
CurrentModule = VMEC
```

# VMEC.jl
`VMEC.jl` is a Julia package for interfacing with the **V**ariational **M**oments **E**quilibrium **C**ode [(VMEC)](https://github.com/ORNL-Fusion/PARVMEC), a Fortran 77/90 program originally written by Hirshman *et al.*[^1] for three-dimensional ideal magnetohydrodynamic (MHD) equilibrium calculations.
This package includes methods for defining inputs for and running a VMEC calculation and methods and data structures to examine and use the output of VMEC calculations in the Julia REPL and other Julia functions.

## Installation
`VMEC.jl` is available through the Julia package manager (by pressing `]` in the REPL):
```julia
(@v1.7) > add VMEC
julia> using VMEC
```
The Fortran source code is packaged as a binary through the [`VMEC_jll.jl`](https://github.com/JuliaBinaryWrappers/VMEC_jll.jl) package.
While the core routines in `VMEC_jll.jl` are unchanged, the [source code](https://gitlab.com/wistell/VMEC2000/-/tree/v1.0.3) for the binary has been modified to allow for in-memory interfacing with Julia.
As such, VMEC can called directly from Julia to produce new equilibria (see [Running VMEC from Julia](@ref)), either through the REPL or as part of other Julia programs.

> **Caveat:** The ability to run VMEC from Julia is architecture dependent.
> Currently running VMEC from Julia is only available on x86-64 architectures running either Linux or MacOS.  
> Due the dependence of `VMEC_jll.jl` on other binaries, specifically the binary for [ScaLAPACK](https://www.netlib.org/scalapack/), `VMEC_jll.jl` is currently unavailable on Windows.
> A `VMEC_jll.jl` binary artifact is built for Windows, however it does not contain the VMEC source code.
> As soon as the appropriate Windows binaries become available, the Windows `VMEC_jll.jl` artifact will include the Fortran routines.
> See [VMEC_jll](ref) for more details.

## Usage
The `VMEC.jl` package has routines and interfaces that can be used to:
  - Run a VMEC equilibrium calculation and store the results, see [Running VMEC from Julia](@ref)
  - Load the output from an existing VMEC equilibrium, see [Loading VMEC output](@ref)
  - Calculate the value of equilibrium magnetic field and geometry quantities at any point in the simulation volume, see [Working with VMEC data](@ref).
The output of a VMEC calculation can then be used by other routines requiring geometry information data, see for example the [``\Gamma_c``](https://wistell.gitlab.io/GammaC.jl) calculation for energetic particle transport and the [PlasmaTurbulenceSaturationModel.jl](https://bfaber.gitlab.io/PlasmaTurbulenceSaturationModel.jl) for turbulence saturation calculations.

## Theory
VMEC solves the ideal MHD force balance equation
```math
\mathbf{J} \times \mathbf{B} = \nabla P,
```
subject to the divergence free condition and Ampere's law:
```math
\nabla \cdot \mathbf{B} = 0,\,\,\,\nabla \times \mathbf{B} = \mu_0 \mathbf{J}.
```

### Coordinate system
VMEC assumes both that nested magnetic flux surfaces exist and the following representation of the magnetic field:
```math
\mathbf{B} = \nabla \psi(s) \times \nabla \left(\theta_v - \iota(s) \zeta_v\right),
```
where ``\psi(s)`` is toroidal flux at the surface ``s \in [0,1]`` with ``s = 1`` corresponding to the last closed flux surfaces.
The coordinate ``\zeta_v`` is the negative of the right-handed cylindrical toroidal angle, ``\theta_v`` is a poloidal-like angle and ``\iota(s)`` is the rotational transform.
This coordinate system follows magnetic field lines and is *left-handed* when the assumption that the poloidal angle increases moving over the top from the outside to the inside of the torus when viewed from above.

The coordinate ``\theta_v`` is related to the normal poloidal angle ``\theta \in [0, 2\pi)`` by the nonlinear equation ``\theta = \theta_v + \lambda(\theta_v, \zeta_v)``.
The function ``\lambda`` is used as a regularization parameter to reduce the power in the largest Fourier harmonics to improve numerical accuracy[^2].
Physical quantities, such as the ``(R, Z)`` coordinates of the last closed flux surface of the magnetic field ``\mathbf{B}``, in VMEC are defined using this modified Fourier representation:
```math
A\left(s, \theta_v, \zeta_v\right) = \sum\limits_{m, n} \tilde{A}(s)^c_{m, n} \cos\left(m \theta_v - N n \zeta_v\right) + \tilde{A}(s)^s_{m, n}\sin\left(m \theta_v - N n \zeta_v\right).
```
Here ``N`` is the number of field periods defining the stellarator geometry.
For geometries with *stellarator symmetry*, quantities will be defined by either a ``\sin`` or ``\cos`` series depending on the symmetry of the quantity.
For example, the cylindrical ``R`` coordinate will be only a function of the ``\tilde{R}(s)^c_{m, n}`` amplitudes, while the cylindrical ``Z`` coordinate will be a function of only the ``\tilde{Z}^s_{m, n}`` amplitudes.


### MHD energy principle
VMEC solves for an MHD equilibrium for a given last closed flux surface (requiring the specification of the boundary coefficients for ``R\left(\theta_v, \zeta_v\right)`` and ``Z\left(\theta_v, \zeta_v\right)`` by minimizing the ideal MHD energy functional[^3]:
```math
\int d\mathbf{x} \frac{B^2}{2\mu_0} + \frac{p}{\gamma-y}
```
where ``d\mathbf{x}`` is the differential volume element, ``p`` is the isotropic pressure and ``\gamma`` is the adiabatic index.
VMEC solves this equation in the inverse representation ``\mathbf{x} = \mathbf{x}\left(s, \theta_v, \zeta_v\right)`` and ``d\mathbf{x} = \sqrt{g}ds d\theta_v d\zeta_v`` where ``\sqrt{g}`` is the Jacobian of the coordinate transformation and by using both the covariant and contravariant forms of the magnetic field: ``\mathbf{B} = B^s \mathbf{e}_s + B^{\theta_v} \mathbf{e}_{\theta_v} + B^{\zeta_v} \mathbf{e}_<\zeta_v}= B_s \nabla s + B_{\theta_v} \nabla \theta_v + B_{\zeta_v} \nabla \zeta_v``.
For an initial parameterization of the last closed flux surface, VMEC uses a gradient descent algorithm to calculate the components of magnetic field, jacobian, and coordinate basis vectors interior to the plasma that minimize the MHD energy principle.

[^1]: [S. P. Hirshman and J. C. Whitson, Phys. Fluids 26, 3553 (1983)](https://aip.scitation.org/doi/10.1063/1.864116)
[^2]: [S. P. Hirshman and H. K. Meier, Phys. Fluids 28, 1387 (1985)](https://aip.scitation.org/doi/10.1063/1.864972)
[^3]: [I. B. Berstein *et al.*, Proc. Royal Soc. London. Series A. Math. and Phys. Sciences, 244, 1236, pp. 17-40 (1958)](https://doi.org/10.1098/rspa.1958.0023)
