# Public API

## [Types](@id Types_public)
```@autodocs
Modules = [VMEC]
Private = false
Pages = ["VmecTypes.jl"]
```

## [Fortran Interface](@id Fortran_public)
```@autodocs
Modules = [VMEC]
Private = false
Pages = ["FortranInterface.jl", "NamelistInput.jl", "NetCDFUtils.jl"]
```

## [Coordinate Transformations](@id Coordinate_public)
```@autodocs
Modules = [VMEC]
Private = false
Pages = ["Coordinates.jl", "Boozer.jl"]
```

## [Fourier Transform Utilities](@id Fourier_public)
```@autodocs
Modules = [VMEC]
Private = false
Pages = ["FourierTransforms.jl"]
```
