### A Pluto.jl notebook ###
# v0.17.5

using Markdown
using InteractiveUtils

# ╔═╡ 2d3a6ae5-34f4-47a3-b15e-47dbc0d5c933
begin
	#If you wish to install these packages into your base Julia environment, create a block comment around lines 4 - 12 by making line 3 read "#=" and line 13 read "=#". Otherwise the packages will be installed to a temporary local directory and cleaned up upon exiting this notebook.
#
	using Pkg
	Pkg.activate(mktempdir())

	# We need JSServe to render in the notebook
	Pkg.add("JSServe")
	Pkg.add("WGLMakie")
	Pkg.add("PlasmaEquilibriumToolkit")
	Pkg.add("VMEC")
	Pkg.add("Interpolations")
#
	using JSServe, WGLMakie, PlasmaEquilibriumToolkit, VMEC, Interpolations
	WGLMakie.activate!()
	# The Page() function will return the correct environment for plotting 
	Page()
end

# ╔═╡ ea7bd676-1957-4e89-b17d-2504422a3b60
Pkg.add(url="https://gitlab.com/wistell/GammaC.jl", rev="master"); using GammaC

# ╔═╡ 4f75103c-c18f-4643-ac1d-98b3d3bafb48
using Downloads; Downloads.download("https://gitlab.com/wistell/VMEC.jl/-/raw/master/test/wout_aten.nc");

# ╔═╡ 6e82da80-7ac8-11ec-2f3a-71debe223890
md"""
# Getting started with the `PlasmaEquilibriumToolkit.jl` and `VMEC.jl`
Let's start by loading up the appropriate packages from working magnetic coordinates, VMEC files and plotting.

We will install the [PlasmaEquilibriumToolkit](https://wistell.gitlab.io/PlasmaEquilibriumToolkit.jl/dev) and [VMEC](https://wistell.gitlab.io/VMEC.jl/gitlab_pages) packages to define magnetic coordinates and to work with VMEC data. The [WGLMakie](https://makie.juliaplots.org/dev/documentation/backends/wglmakie/) package will be used for plotting purposes. The following commands will load the appropriate packages; be patient, this may take a while to precompile the `JSServe` and `WGLMakie` packages.
"""

# ╔═╡ 638051f2-5298-424d-9b89-23042b92acaa
md"""
### Loading VMEC data
To demonstrate the capabilities of the `PlasmaEquilibriumToolkit` and `VMEC`, let's download a NetCDF file for the [ATEN](https://doi.org/10.1017/S0022377820000963) equilibrium from the WISTELL Gitlab.
"""

# ╔═╡ 511588f9-11e1-4ce9-bd6d-002d33dd4318
md"""
This equilibrium can be readily loaded using the `readVmecWout` function.  It returns two different objects containing VMEC data, which we will call `vmec` and `vmec_data`, though these names are arbitrary.
  - `vmec`: this is an object of type `Vmec`, where the radial dependence of VMEC quantities has been interpolated over the plasma minor radius using a cubic B-spline from the [Interpolations](https://github.com/JuliaMath/Interpolations.jl) package.  This also includes the individual mode amplitudes for spectral data, where each field containing spectral data now holds a spline interpolation for *both* the ``cos`` and ``sin`` Fourier series.
  - `vmec_data`: this is an object of type `VmecData`, which has a one-to-one correspondence with the Fortran output of a VMEC calculation.  The ``sin`` and ``cos`` components of spectral data are kept seperately.  This data is used create `Vmec` type objects and rarely will the underlying `VmecData` object need to be used.
"""

# ╔═╡ b20c5d04-c96d-4c62-8973-51f9bfcf4f45
# If the vmec_data field is not desired, use an underscore to collect the output
# -> vmec, _ = readVmecWout("wout_aten.nc")
vmec, vmec_data = readVmecWout("wout_aten.nc");

# ╔═╡ 7c0a6c61-2f4f-4ad5-8a6c-e3c77954bdb0
md"""
### VMEC.jl data types
The fields of the `Vmec` data type are labeled to reflect the VMEC data from which they are derived.  The most important labelling change is that in the `Vmec` types, the designation for ``sin`` and ``cos`` amplitudes has been dropped, as both are now contained within a `Vmec` field for spectral data.  For stellarator symmetric equilibria, the non-stellarator symmetric modes will have zero amplitude. To see all the possible fieldnames of a `Vmec` object, we can use the `fieldnames` function:
"""

# ╔═╡ 3050951f-08c9-46c4-99b1-6e795eb53b4c
fieldnames(Vmec)

# ╔═╡ c0a0e412-255a-4975-a8be-492f292ebecd
md"""
As we can see, for quantities the radial cylindrical coordinate, VMEC arrays `rmnc` and `rmns` have been combined into a single `rmn` field.  We can look at the form of the data for these spectral fields, let's take a look at the λ parameter used in the VMEC spectral condensation algorithm as an example.
"""

# ╔═╡ 53b35758-6144-4b08-a63b-37a33c3498f0
typeof(vmec.λmn)

# ╔═╡ 1a5e214f-2a71-4362-b466-587ee03ff8c4
md"""
We can see here it's a `Vector{VmecFourierSpline{Float64}}`.  The `VmecFourierSpline` type is a custom type that holds the poloidal and toroidal mode numbers and ``sin`` and ``cos`` mode amplitude interpolated across the plasma minor radius.  The structure is defined below:
```julia
struct VmecFourierSpline{T}
	m::T
	n::T
	cos::Interpolations.Extrapolation
	sin::Interpolations.Extrapolation
end
```

For the ATEN VMEC equilibrium loaded here, we have ``M_{pol} = 8``, ``N_{tor} = 8``, and the application of the reality condition for the Fourier series representation in VMEC is such that only half of the *poloidal* mode numbers are used.
"""

# ╔═╡ 5d2ead78-65d3-4f3c-907e-260bc94e36cc
(vmec.mpol, vmec.ntor)

# ╔═╡ 526e785a-52ec-4d09-9b9c-3570748acc1c
md"""
With these mode resolutions, the 11th entry of `vmec.λmn` should correspond to (m = 1, n = -28).  The toroidal mode numbers already include the multiplicative factor the number of field periods, which in the case of ATEN is 4, and can be accessed through the `nfp` field. 
"""

# ╔═╡ bec96fc5-01bb-4468-9bec-1ce300e23f22
vmec.nfp == 4

# ╔═╡ 2d4b9e8a-19bb-4d65-a386-321e7cf8149e
(vmec.λmn[11].m, vmec.λmn[11].n) == (1.0, -28.0)

# ╔═╡ fc37fe05-2de9-4578-b3e9-a0144dd405b6
md"""
For fields that are not represented spectrally, such as the rotational transform ``\iota(s)``, the representation is just a cubic B-spline.
"""

# ╔═╡ eb3dc017-8358-4aee-9f1b-0fe2da944e04
typeof(vmec.iota) <: Interpolations.Extrapolation

# ╔═╡ cbaad512-abc2-490e-8090-633bb45c2ce9
md"""
The cubic B-spline interpolations for the radial dependence allow for us to determine both the value of the interpolation at any point s ∈ (0, 1) and the value of the derivative with restpect to s.  We can demonstrate this by plotting both the value of the rotational transform across the plasma minor radius and the value of its derivative.
"""

# ╔═╡ 997633db-8a6a-4974-a57f-ec8d6baa8594
begin
	fig = Figure(resolution =  (500, 300))
	ax1 = Axis(fig[1,1], xlabel = "s", xlims = [0, 1], ylabel = "ι(s)", yticklabelcolor = :blue, ylabelcolor = :blue, ygridvisible = false)
	ax2 = Axis(fig[1,1], xlims = [0, 1], ylabel = "dι/ds", yaxisposition = :right, yticklabelcolor = :red, ylabelcolor = :red, ygridvisible = false)
	hidespines!(ax2)
	hidexdecorations!(ax2)

	s = 0.01:0.01:0.99
	lines!(ax1, s, map(i -> vmec.vp(i), s), color = :blue)
	lines!(ax2, s, map(i -> Interpolations.gradient(vmec.vp, i)[], s), color = :red)
	fig
end

# ╔═╡ c466d49b-806a-4b24-ae6b-32de2acf42a0
md"""
As a fun exercise, and to demostrate the interactive power of Pluto.jl, try using the code above to plot the VMEC parameter ``V'``, which is represented in the `Vmec` type as field `vp`.  To do this, change field `iota` in lines 9 and 10 to `vp`.  As ``dV'/ds`` is a measure of the magnetic well (when ``dV'/ds < 0``), we can see that a magnetic well exists in the ATEN equilibrium for ``s \lesssim 0.4``.
"""

# ╔═╡ 456ff9ea-68f8-4631-b0f5-331303317339
md"""
To improve performance of quantities that are defined local to a radial (or flux surface) location, another custom datatype called `VmecSurface` has been defined.  This data type contains only data defined at a desired surface by calling the interpolating function for the radial data.  Each data field in `VmecSurface` has a name that maps to the same data field in `Vmec`, however for the `VmecSurface` type, the data is now just the value at the surface *and* the value of the derivative with respect to ``s`` at the surface.  This can be seen, for example, by creating a `VmecSurface` and looking at the value of the rotational transform.
"""

# ╔═╡ 62965cee-b3fb-490c-b779-f8e8833086e7
# Define a VmecSurface at s = 0.5
vmec_surface = VmecSurface(0.5, vmec);

# ╔═╡ f64f55ac-131e-4461-be57-da3d390e09aa
# The rotational transform should now only have two components, the first is the value at the surface, the second is the value of the derivative at the surface
vmec_surface.iota

# ╔═╡ fc345c14-a0dd-4659-b36a-dc1f94de11a5
md"""
Similarly for spectral data, the splines in the `VmecFourierSpline` type has been reduced to a `VmecFourierData` type, which has 6 fields:
```julia
struct VmecFourierData{T}
	m::T
	n::T
	cos::T
	sin::T
	dcosds::T
	dsinds::T
end
```
We can see what this looks like for the 11th entry of the `vmec.λmn` field:
"""

# ╔═╡ 41953a8f-ea35-4ee9-b320-1a7fd86f2d37
vmec_surface.λmn[11]

# ╔═╡ 915f1ded-d6cf-45fe-8bc9-25a865dd8cc8
md"""
By using this immutable datatype, the performance of the inverse Fourier transform functions that are required to calculate geometric quantities in real space is significantly improved.  An example of these functions will be seen below.
"""

# ╔═╡ 4d7bce0e-b8cc-4b3a-89bc-e4c22204d96c
md"""
## Using the `PlasmaEquilibriumToolkit`
The `PlasmaEquilibriumToolkit` attempts to provide a standardized interface for generating and maniuplating geometric data produced by MHD equilibrium solvers.  While this package is still under very active development, some of the most important current features are listed below:
  - Providing definitions for standard magnetic coordinate systems.  Currently implemented are:
    - PEST coordinates - ``\mathbf{B} = \nabla \psi \times \nabla(\alpha - \iota \zeta)`` where ``\psi`` is the signed *toroidal* flux divided by ``2\pi`` oriented in the clockwise direction, ``\alpha`` is the field line label, and ``\zeta`` is the geometric toroidal angle that increases in the clockwise direction.
    - Flux coordinates - ``\mathbf{B} = \nabla \psi \times \nabla(\theta - \iota \zeta)``, where ``\psi`` is the signed *toroidal* flux divided by ``2\pi`` oriented in the clockwise direction, ``\theta`` is the straight field line poloidal angle that increases when traversed over the top of the torus when viewed from above, and ``\zeta`` is the toroidal angle that increases in the clockwise direction.
    - Boozer coordinates - ``\mathbf{B} = \nabla \psi \times \nabla \chi - \iota \nabla \psi \times \nabla \varphi`` where ``\psi`` is flux label, ``\chi`` is Boozer poloidal-like angle, and ``\varphi`` is the Boozer toroidal-like angle.
  - Providing methods for performing a change of basis transformations between different magnetic coordinate representations, as well as to standard cylindrical and Cartesian coordinates.  Methods are defined for treating both covariant and contravariant basis vectors.
  - Providing abstractions for performing the change of basis transformations over arrays of coordinates, making use of multithreading to improve performance.
  - Defining an API for magnetic equilibrium packages such as `VMEC.jl` to provide concrete implementations to produce quantities like `∇B` or curvature `κ` for use in physics analysis.
"""

# ╔═╡ 18903931-e086-4e74-9763-8376af27a475
md"""
### Calculating geometric quantities using magnetic coordinates
To demonstrate the power this approach and to provide a concrete example of how to use methods from the `PlasmaEquilibriumToolkit` to calculate quantities of interest, let's calculate various geometric quantities like the magnetic field magnitude or the magnetic curvatures on surfaces and fieldlines.  The PEST coordinate system provides a natural way of defining a magnetic field, so we can use some of the constructors from the `PlasmaEquilibriumToolkit` to do this:
"""

# ╔═╡ 0a33a3dd-a6a4-49d6-9606-4948d89ff4a5
# Define the toroidal flux value, the field line label and the range of points we want to use for a field line
ψ = 0.5 * vmec.phi(1.0)/2π * vmec.signgs; α = 0.0; ζ = -2π:2π/127:2π;

# ╔═╡ dab83a8c-12bc-4c90-8bb3-f11e63983fe1
fl = MagneticCoordinateCurve(PestCoordinates, ψ, α, ζ);

# ╔═╡ 1de5c322-ddd5-4a6f-8c90-21f8cf8ca19e
md"""
The return value of the `MagneticCoordinateGrid` and `MagneticCoordinateCurve` functions are a `StructArray` object, in this case a `StructArray{PestCoordinates}`.
"""

# ╔═╡ af2d540c-58cd-4a56-9026-13b9b4473864
typeof(fl)

# ╔═╡ a39b4c37-d7e4-4956-be51-aaa457e8838d
md"""
By using a `StructArray`, we can access the individual fields of the coordinates using the `.` derefencing syntax, e.g. to get all of the ζ values we can simply use `fl.ζ`.
"""

# ╔═╡ 7bfe99a5-733a-4b89-87e2-02cc24c49b98
collect(fl.ζ)  # We use collect because ζ is defined as a range

# ╔═╡ 3344cbe2-3d1a-4dd4-8c0d-bbb7f08d4f9a
md"""
To calculate geomtric information from VMEC at the points specified along the fieldline `fl`, we must first know what the equivalent internal VMEC coordinates are.  This means we have to solve for the root of ``\theta = \theta_v + \lambda(\theta_v, \zeta_v)``, where ``\theta`` is the straight field line poloidal angle, ``\theta_v`` is the internal VMEC poloidal angle, and ``\zeta_v`` is the internal VMEC toroidal angle and is equivalent to the geometric cylindrical toroidal angle.  Thankfully, it is quite easy to both do this transformation using methods from `VMEC.jl` and to do it over a collection of points.  The basic syntax for doing coordinate transformations follows that of [CoordinateTransformations.jl](https://github.com/JuliaGeometry/CoordinateTransformations.jl).  Coordinate transformations are defined as a *singleton* type, for example the transformation to internal VMEC coordinates from PEST coordinates:
```julia
struct VmecFromPest; end <: Transformation
```
where `Transformation` is an abstract type defined by `CoordinateTransformations.jl`.
`VMEC.jl` provides the concrete implementation of this transformation and automatically solves for the internal VMEC poloidal angle for a given point in magnetic coordinates.  To accomplish this transformation, which is local to a flux surface, information about the flux surface is required.  The standard way of doing this is to pass a `VmecSurface` object along with the desired coordinates to be transformed.  **Caveat**: currently all coordinates must have the same flux surface label and that label must match the `VmecSurface` flux surface label.  There is no currently no check to ensure this is not violated, so it is up to the user to ensure accurate transformations are being performed.  The signature for this coordinate transformation (and other similar coordinate transformations) is given below:
```julia
v = VmecFromPest()(x::PestCoordinates, vmec_surface::VmecSurface)
```
This will produce the coordinate `v` in internal `VMEC` coordinates corresponding to `x` given in `PestCoordinates` provided that `x.ψ` correlates to the same flux surface as `vmec_surface.s`, i.e. `x.ψ == s * vmec_surface.phi[end]/2π * vmec_surface.signgs`.  Additionally, thanks to some of the abstractions in the `PlasmaEquilibriumToolkit`, this transformation can executed seamlessly over an entire range of points with the same transformation signature.
"""

# ╔═╡ f2492c1c-d39b-42a6-8b12-fd1210bcedd4
v = VmecFromPest()(fl, vmec_surface);

# ╔═╡ d3f5ab4c-11aa-4eae-96a5-20d2eb57def8
md"""
Now `v` should be an array of `VmecCoordinates`.
"""

# ╔═╡ c6fee6d0-1186-489c-8f58-b0dc4960f8c8
typeof(v)

# ╔═╡ dc1e277a-0936-4b46-8dae-d51c0fb00b8e
md"""
With the `VmecCoordinates` defined, these coordinates can now be used to calculate ``|B|`` along the field line using the `inverseTransform` function and the `vmec_surface.bmn` field:
"""

# ╔═╡ 8c9e25b4-4a3a-4889-a867-a5a11a0735a4
B_mag = inverseTransform(v, vmec_surface.bmn)

# ╔═╡ 21452b98-480a-4e98-ac8a-5b64113ad5da
md"""
Additionally, if we wanted the derivatives of ``|B|`` with respect to either ``s``, ``\theta_v`` or ``\zeta_v``, we can use the `deriv` keyword argument in `inverseTransform` which is valid for the keyword values `:ds`, `:dθ`, and `:dζ`:
"""

# ╔═╡ 4e93e58f-789d-4cde-9104-f5cedd0ce7d5
dBdζ = inverseTransform(v, vmec_surface.bmn; deriv = :dζ)

# ╔═╡ 16ba8a2a-fc8b-48c2-83f7-8cef1e354f71
md"""
Let's plot ``|B|`` to make sure we did things correctly.
"""

# ╔═╡ 7769fd6a-fd8f-4c80-8c73-bddd6f83e10a
begin
	fig2 = Figure(resolution =  (500, 300))
	ax11 = Axis(fig2[1,1], xlabel = "ζ", xlims = [first(fl.ζ), last(fl.ζ)], ylabel = "|B|")
	lines!(ax11, fl.ζ, B_mag, color = :red)
	fig2
end

# ╔═╡ dc038728-3cf4-4e5b-91e6-c64a7f46fac2
md"""
Looks good!  We can using a similar process, we can generate magnetic surface in straight field line coordinates and plot the magnitude of the magnetic field there.
"""

# ╔═╡ 5e4f936e-4cd1-4be6-9eca-5cb45b2eb5b2
begin
  Ψ = 0.99 * vmec_surface.phi[end]/2π * vmec_surface.signgs; θ = 0:2π/128:2π; ϕ = 0:2π/128:2π;
  flux_surf = MagneticCoordinateGrid(FluxCoordinates, ψ, θ, ϕ)
  vmec_coords = VmecFromFlux()(flux_surf, vmec_surface);
  Bmag = [VMEC.inverseTransform(i, vmec_surface.bmn) for i in vmec_coords];
  cart_coords = CartesianFromVmec()(vmec_coords, vmec_surface);
  X = [q[1] for q in cart_coords];
  Y = [q[2] for q in cart_coords];
  Z = [q[3] for q in cart_coords];
  fig3 = Figure()
  lscene = LScene(fig3[1,1])
  surf = surface!(lscene, X,Y,Z, color = [B for B in Bmag], colormap = :plasma);
  #zoom!(lscene.scene,10.0)
  legend = Colorbar(fig3[1, 2], surf, label = "|B|");
  fig3
end

# ╔═╡ f16d6d9a-efb2-4751-9ccf-e13694808465
md"""
## Putting it all together: the ``\Gamma_c`` metric and quasisymmetry deviation
We can see this how this is used in a straightforward manner by looking at the [GammaC.jl](https://gitlab.com/wistell/GammaC.jl) package as an example.
"""

# ╔═╡ dd4a4643-6233-4b39-bc78-fe319754c831
md"""
To call the routine to compute the ``\Gamma_c`` and ``\epsilon^{eff}`` metrics on flux surface, we can use the method
```julia
function GammacTarget(vmecSurface::VmecSurface,
					  zetaMin::Float64, 
	                  zetaStep::Float64, 
	                  zetaMax::Float64,
                      BResolution::Int64, 
	                  epsEff::Bool=true, 
	                  gammac::Bool=true)
```
We will use the ATEN `VmecSurface` we have already created in the evaluation and evaluate it over 50 toroidal transits for 100 magnetic field values.
"""

# ╔═╡ bd0ac2a1-a245-4bb7-9545-d88ca5e3657f
γ, ϵ = GammaC.GammacTarget(vmec_surface, 0.0, 0.01, 50π, 100)

# ╔═╡ 2082a490-0655-4708-b067-3678e9b4d4fe
md"""
It's instructive to look at the source code to see how the different components of the `PlasmaEquilibriumToolkit.jl` and `VMEC.jl` are used in the function `setupGammaCSplines`:
```julia
  @inbounds for i = 1:nZeta
    #Find the coordinates and perform coordinate transformations for each point along the field line
    pestCoords = PestCoordinates(vmec.signgs*vmec.phi[3]*vmec.s/(2*π),0.0,zetas[i])
    vmecCoords = VmecFromPest()(pestCoords,vmec)
    vmecContraBasis = basis_vectors(Contravariant(),CartesianFromVmec(),vmecCoords,vmec)
    pestContraBasis = transform_basis(PestFromVmec(),vmecCoords,vmecContraBasis,vmec)
    fluxContraBasis = transform_basis(FluxFromVmec(),vmecCoords,vmecContraBasis,vmec)
    fluxCoBasis = transform_basis(CovariantFromContravariant(),fluxContraBasis)

    #Calculate the derived quantities
    eThetaMag[i] = abs(fluxCoBasis,2)
    gradPsiMag[i] = abs(pestContraBasis,1)
    grad_B = gradB(vmecCoords,vmecContraBasis,vmec)
    geodesicCurvature[i] = curvatureComponents(pestContraBasis,grad_B)[2]
    Bmag[i] = inverseTransform(vmecCoords,vmec.bmn)
    dBdPsi[i] = twoPiEdgeFlux*inverseTransform(vmecCoords,vmec.bmn,deriv=:ds)
    BsupZeta[i] = inverseTransform(vmecCoords,vmec.bsupvmn)
    dBsupZetadPsi[i] = twoPiEdgeFlux*inverseTransform(vmecCoords,vmec.bsupvmn,deriv=:ds)
    gradZeta = pestContraBasis[:,3]
    Bxyz = cross(pestContraBasis[:,1],pestContraBasis[:,2])

    #calculate the term in the parentheses in dVdb (Nem eq 35)
    VT1 = chiPrime*dot(cross(pestContraBasis[:,1],Bxyz),gradZeta)/(Bmag[i]*gradPsiMag[i])
    VT2 = 2 * dBdPsi[i] - (Bmag[i] * dBsupZetadPsi[i]) / BsupZeta[i]
    VT[i] = VT1 - VT2
  end
```
We can see many of the elements of the `PlasmaEquilibriumToolkit.jl` and `VMEC.jl` in action and can serve as inspiration for using these packages to quickly construct new physics metrics.  We can also quickly calculate and plot the quasisymmetry deviation for the ATEN equilibrium using the `VMEC.quasisymmetryDeviation` function:
```julia
function quasisymmetryDeviation(mQS::Int,
                                nQS::Int,
                                mBoozer::Int,
                                nBoozer::Int,
                                vmecSurfaces::AbstractVector{VmecSurface{T}};
                               ) where {T}
```
"""

# ╔═╡ f09ffb51-1207-4553-80e6-f87121d53880
begin
	s2 = 0.05:0.05:0.95
	surfs = [VmecSurface(si, vmec) for si in s2]
	qs = quasisymmetryDeviation(1, 4, 16, 16, surfs)
	fig4 = Figure(resolution =  (500, 300))
	ax21 = Axis(fig4[1,1], xlabel = "s", xlims = [0, 1], ylabel = "QS deviation")
	lines!(ax21, s2, qs, color = :red)
	fig4
end

# ╔═╡ Cell order:
# ╟─6e82da80-7ac8-11ec-2f3a-71debe223890
# ╠═2d3a6ae5-34f4-47a3-b15e-47dbc0d5c933
# ╟─638051f2-5298-424d-9b89-23042b92acaa
# ╠═4f75103c-c18f-4643-ac1d-98b3d3bafb48
# ╟─511588f9-11e1-4ce9-bd6d-002d33dd4318
# ╠═b20c5d04-c96d-4c62-8973-51f9bfcf4f45
# ╟─7c0a6c61-2f4f-4ad5-8a6c-e3c77954bdb0
# ╠═3050951f-08c9-46c4-99b1-6e795eb53b4c
# ╟─c0a0e412-255a-4975-a8be-492f292ebecd
# ╠═53b35758-6144-4b08-a63b-37a33c3498f0
# ╟─1a5e214f-2a71-4362-b466-587ee03ff8c4
# ╠═5d2ead78-65d3-4f3c-907e-260bc94e36cc
# ╟─526e785a-52ec-4d09-9b9c-3570748acc1c
# ╠═bec96fc5-01bb-4468-9bec-1ce300e23f22
# ╠═2d4b9e8a-19bb-4d65-a386-321e7cf8149e
# ╟─fc37fe05-2de9-4578-b3e9-a0144dd405b6
# ╠═eb3dc017-8358-4aee-9f1b-0fe2da944e04
# ╟─cbaad512-abc2-490e-8090-633bb45c2ce9
# ╠═997633db-8a6a-4974-a57f-ec8d6baa8594
# ╟─c466d49b-806a-4b24-ae6b-32de2acf42a0
# ╟─456ff9ea-68f8-4631-b0f5-331303317339
# ╠═62965cee-b3fb-490c-b779-f8e8833086e7
# ╠═f64f55ac-131e-4461-be57-da3d390e09aa
# ╟─fc345c14-a0dd-4659-b36a-dc1f94de11a5
# ╠═41953a8f-ea35-4ee9-b320-1a7fd86f2d37
# ╟─915f1ded-d6cf-45fe-8bc9-25a865dd8cc8
# ╟─4d7bce0e-b8cc-4b3a-89bc-e4c22204d96c
# ╟─18903931-e086-4e74-9763-8376af27a475
# ╠═0a33a3dd-a6a4-49d6-9606-4948d89ff4a5
# ╠═dab83a8c-12bc-4c90-8bb3-f11e63983fe1
# ╟─1de5c322-ddd5-4a6f-8c90-21f8cf8ca19e
# ╠═af2d540c-58cd-4a56-9026-13b9b4473864
# ╟─a39b4c37-d7e4-4956-be51-aaa457e8838d
# ╠═7bfe99a5-733a-4b89-87e2-02cc24c49b98
# ╟─3344cbe2-3d1a-4dd4-8c0d-bbb7f08d4f9a
# ╠═f2492c1c-d39b-42a6-8b12-fd1210bcedd4
# ╟─d3f5ab4c-11aa-4eae-96a5-20d2eb57def8
# ╠═c6fee6d0-1186-489c-8f58-b0dc4960f8c8
# ╟─dc1e277a-0936-4b46-8dae-d51c0fb00b8e
# ╠═8c9e25b4-4a3a-4889-a867-a5a11a0735a4
# ╟─21452b98-480a-4e98-ac8a-5b64113ad5da
# ╠═4e93e58f-789d-4cde-9104-f5cedd0ce7d5
# ╟─16ba8a2a-fc8b-48c2-83f7-8cef1e354f71
# ╠═7769fd6a-fd8f-4c80-8c73-bddd6f83e10a
# ╟─dc038728-3cf4-4e5b-91e6-c64a7f46fac2
# ╠═5e4f936e-4cd1-4be6-9eca-5cb45b2eb5b2
# ╟─f16d6d9a-efb2-4751-9ccf-e13694808465
# ╠═ea7bd676-1957-4e89-b17d-2504422a3b60
# ╟─dd4a4643-6233-4b39-bc78-fe319754c831
# ╠═bd0ac2a1-a245-4bb7-9545-d88ca5e3657f
# ╟─2082a490-0655-4708-b067-3678e9b4d4fe
# ╠═f09ffb51-1207-4553-80e6-f87121d53880
