@testset "VMEC Surface Tests" begin
  vmec = VMEC.readVmecWout(joinpath(@__DIR__, "wout_circular_tokamak.nc"));
  s = 0.7
  vmecsurf = magnetic_surface(s, vmec)
  rtol = 1.0E-6
  @testset "Test scalar quantities" begin
    @test isapprox(vmecsurf.s, s, rtol=rtol)
    @test vmecsurf.nfp == vmec.nfp
    @test vmecsurf.mpol == vmec.mpol
    @test vmecsurf.ntor == vmec.ntor
    @test vmecsurf.mnmax == vmec.mnmax
    @test vmecsurf.mnmax_nyq == vmec.mnmax_nyq
    @test vmecsurf.Aminor_p == vmec.Aminor_p
    @test vmecsurf.Rmajor_p == vmec.Rmajor_p
    @test vmecsurf.aspect == vmec.aspect
    @test vmecsurf.lasym == vmec.lasym
  end
  @testset "Test structural vectors" begin
    @test vmecsurf.xm == vmec.xm
    @test vmecsurf.xn == vmec.xn
    @test vmecsurf.xm_nyq == vmec.xm_nyq
    @test vmecsurf.xn_nyq == vmec.xn_nyq
  end
  @testset "Verify evaluated quantities" begin
    @test vmecsurf.phi[1] == vmec.phi(s)
    @test vmecsurf.chi[1] == vmec.chi(s)
    @test vmecsurf.pres[1] == vmec.pres(s)
    @test vmecsurf.iota[1] == vmec.iota(s)
    @test vmecsurf.beta_vol[1] == vmec.beta_vol(s)
    @test vmecsurf.jcuru[1] == vmec.jcuru(s)
    @test vmecsurf.jcurv[1] == vmec.jcurv(s)
    @test vmecsurf.mass[1] == vmec.mass(s)
    @test vmecsurf.buco[1] == vmec.buco(s)
    @test vmecsurf.bvco[1] == vmec.bvco(s)
    @test vmecsurf.vp[1] == vmec.vp(s)
    @test vmecsurf.specw[1] == vmec.specw(s)
    @test vmecsurf.over_r[1] == vmec.over_r(s)
    @test vmecsurf.jdotb[1] == vmec.jdotb(s)
    @test vmecsurf.bdotb[1] == vmec.bdotb(s)
    @test vmecsurf.bdotgradv[1] == vmec.bdotgradv(s)
    @test vmecsurf.DMerc[1] == vmec.DMerc(s)
    @test vmecsurf.DShear[1] == vmec.DShear(s)
    @test vmecsurf.DWell[1] == vmec.DWell(s)
    @test vmecsurf.DGeod[1] == vmec.DGeod(s)
    @test vmecsurf.equif[1] == vmec.equif(s)
  end
  @testset "Test a few derivatives" begin
    @test isapprox(vmecsurf.phi[2], 67.86, rtol=rtol)
    @test isapprox(vmecsurf.jcuru[2], 1.7068845172099124e6, rtol=rtol)
  end
  @testset "Test interpolations of other quantities" begin
    for i in 1:vmecsurf.mnmax
      @test vmecsurf.rmn[i].cos == vmec.rmn[i].cos(s)
      @test vmecsurf.rmn[i].sin == vmec.rmn[i].sin(s)
      @test vmecsurf.zmn[i].cos == vmec.zmn[i].cos(s)
      @test vmecsurf.zmn[i].sin == vmec.zmn[i].sin(s)
      @test vmecsurf.λmn[i].cos == vmec.λmn[i].cos(s)
      @test vmecsurf.λmn[i].sin == vmec.λmn[i].sin(s)
    end
    for i in 1:vmecsurf.mnmax_nyq
      @test vmecsurf.bmn[i].cos == vmec.bmn[i].cos(s)
      @test vmecsurf.bmn[i].sin == vmec.bmn[i].sin(s)
      @test vmecsurf.gmn[i].cos == vmec.gmn[i].cos(s)
      @test vmecsurf.gmn[i].sin == vmec.gmn[i].sin(s)
      @test vmecsurf.bsubsmn[i].cos == vmec.bsubsmn[i].cos(s)
      @test vmecsurf.bsubsmn[i].sin == vmec.bsubsmn[i].sin(s)
      @test vmecsurf.bsubumn[i].cos == vmec.bsubumn[i].cos(s)
      @test vmecsurf.bsubumn[i].sin == vmec.bsubumn[i].sin(s)
      @test vmecsurf.bsubvmn[i].cos == vmec.bsubvmn[i].cos(s)
      @test vmecsurf.bsubvmn[i].sin == vmec.bsubvmn[i].sin(s)
      @test vmecsurf.bsupumn[i].cos == vmec.bsupumn[i].cos(s)
      @test vmecsurf.bsupumn[i].sin == vmec.bsupumn[i].sin(s)
      @test vmecsurf.bsupvmn[i].cos == vmec.bsupvmn[i].cos(s)
      @test vmecsurf.bsupvmn[i].sin == vmec.bsupvmn[i].sin(s)
      @test vmecsurf.currumn[i].cos == vmec.currumn[i].cos(s)
      @test vmecsurf.currumn[i].sin == vmec.currumn[i].sin(s)
      @test vmecsurf.currvmn[i].cos == vmec.currvmn[i].cos(s)
      @test vmecsurf.currvmn[i].sin == vmec.currvmn[i].sin(s)
    end
  end

  atol_interp = 1e-7
  rtol_interp = 1e-4
  @testset "Test simple torus" begin
    #Define a simple torus
    R0 = 11.0
    a = 3.0
    rm0 = SurfaceFourierData(0.0, 0.0, 11.0, 0.0, 0.0, 0.0, 0.0, 0.0)
    rm1 = SurfaceFourierData(1.0, 0.0, 3.0, 0.0, 0.0, 0.0, 0.0, 0.0)
    zm0 = SurfaceFourierData(0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0)
    zm1 = SurfaceFourierData(1.0, 0.0, 3.0, 0.0, 0.0, 0.0, 0.0, 0.0)
    rmn = StructVector([rm0, rm1])
    zmn = StructVector([zm0, zm1])
    θres = 101
    ζres = 103
    rInterp, _ = PlasmaEquilibriumToolkit.make_surface_interpolation(rmn, 
                          1, θres, ζres)
    @test length(rInterp.itp.ranges[1]) == θres
    @test length(rInterp.itp.ranges[2]) == ζres
    for θ in range(0, 2π, 16)
      @test isapprox(inverseTransform(VmecCoordinates(0.0, θ, 0.0), rmn),
            rInterp(θ, 0), atol = atol_interp, rtol=rtol_interp)
      @test isapprox(inverseTransform(VmecCoordinates(0.0, θ, 0.0), rmn, deriv=:dθ),
            Interpolations.gradient(rInterp, θ, 0)[1], atol = atol_interp, rtol=rtol_interp)
    end
  end

  function test_splines(vc::VmecCoordinates, vmecsurf::VmecSurface)
    @test isapprox(inverseTransform(vc, vmecsurf.bmn),
                   surface_get(vc, vmecsurf, :b), rtol=rtol_interp,
                   atol=atol_interp)
    @test isapprox(inverseTransform(vc, vmecsurf.rmn),
                   surface_get(vc, vmecsurf, :r), rtol=rtol_interp,
                   atol=atol_interp)
    @test isapprox(inverseTransform(vc, vmecsurf.zmn),
                   surface_get(vc, vmecsurf, :z), rtol=rtol_interp,
                   atol=atol_interp)
    @test isapprox(inverseTransform(vc, vmecsurf.λmn),
                   surface_get(vc, vmecsurf, :λ), rtol=rtol_interp,
                   atol=atol_interp)
    @test isapprox(inverseTransform(vc, vmecsurf.gmn),
                   surface_get(vc, vmecsurf, :g), rtol=rtol_interp,
                   atol=atol_interp)
    @test isapprox(inverseTransform(vc, vmecsurf.bsubsmn),
                   surface_get(vc, vmecsurf, :bsubs), rtol=rtol_interp,
                   atol=atol_interp)
    @test isapprox(inverseTransform(vc, vmecsurf.bsubumn),
                   surface_get(vc, vmecsurf, :bsubu), rtol=rtol_interp,
                   atol=atol_interp)
    @test isapprox(inverseTransform(vc, vmecsurf.bsubvmn),
                   surface_get(vc, vmecsurf, :bsubv), rtol=rtol_interp,
                   atol=atol_interp)
    @test isapprox(inverseTransform(vc, vmecsurf.bsupumn),
                   surface_get(vc, vmecsurf, :bsupu), rtol=rtol_interp,
                   atol=atol_interp)
    @test isapprox(inverseTransform(vc, vmecsurf.bsupvmn),
                   surface_get(vc, vmecsurf, :bsupv), rtol=rtol_interp,
                   atol=atol_interp)
    @test isapprox(inverseTransform(vc, vmecsurf.currumn),
                   surface_get(vc, vmecsurf, :curru), rtol=rtol_interp,
                   atol=atol_interp)
    @test isapprox(inverseTransform(vc, vmecsurf.currvmn),
                   surface_get(vc, vmecsurf, :currv), rtol=rtol_interp,
                   atol=atol_interp)

  end

  function test_exact(vc::VmecCoordinates, vmecsurf::VmecSurface)
    @test isapprox(inverseTransform(vc, vmecsurf.bmn),
                   surface_get_exact(vc, vmecsurf, :bmn), atol=atol_interp)
    @test isapprox(inverseTransform(vc, vmecsurf.rmn),
                   surface_get_exact(vc, vmecsurf, :rmn), atol=atol_interp)
    @test isapprox(inverseTransform(vc, vmecsurf.zmn),
                   surface_get_exact(vc, vmecsurf, :zmn), atol=atol_interp)
    @test isapprox(inverseTransform(vc, vmecsurf.λmn),
                   surface_get_exact(vc, vmecsurf, :λmn), atol=atol_interp)
    @test isapprox(inverseTransform(vc, vmecsurf.gmn),
                   surface_get_exact(vc, vmecsurf, :gmn), atol=atol_interp)
    @test isapprox(inverseTransform(vc, vmecsurf.bsubsmn),
                   surface_get_exact(vc, vmecsurf, :bsubsmn), atol=atol_interp)
    @test isapprox(inverseTransform(vc, vmecsurf.bsubumn),
                   surface_get_exact(vc, vmecsurf, :bsubumn), atol=atol_interp)
    @test isapprox(inverseTransform(vc, vmecsurf.bsubvmn),
                   surface_get_exact(vc, vmecsurf, :bsubvmn), atol=atol_interp)
    @test isapprox(inverseTransform(vc, vmecsurf.bsupumn),
                   surface_get_exact(vc, vmecsurf, :bsupumn), atol=atol_interp)
    @test isapprox(inverseTransform(vc, vmecsurf.bsupvmn),
                   surface_get_exact(vc, vmecsurf, :bsupvmn), atol=atol_interp)
    @test isapprox(inverseTransform(vc, vmecsurf.currumn),
                   surface_get_exact(vc, vmecsurf, :currumn), atol=atol_interp)
    @test isapprox(inverseTransform(vc, vmecsurf.currvmn),
                   surface_get_exact(vc, vmecsurf, :currvmn), atol=atol_interp)
  end

  function test_derivs(vc::VmecCoordinates, vmecsurf::VmecSurface)
    x = inverseTransform(vc, vmecsurf.bmn, deriv=:ds)
    @test isapprox(x, surface_get(vc, vmecsurf, :b ,deriv=:ds),
                   atol = atol_interp, rtol = rtol_interp)
    x = inverseTransform(vc, vmecsurf.bmn, deriv=:dθ)
    @test isapprox(x, surface_get(vc, vmecsurf, :b ,deriv=:dθ),
                   atol = atol_interp, rtol = rtol_interp)
    x = inverseTransform(vc, vmecsurf.bmn, deriv=:dζ)
    @test isapprox(x, surface_get(vc, vmecsurf, :b ,deriv=:dζ),
                   atol = atol_interp, rtol = rtol_interp)
    x = inverseTransform(vc, vmecsurf.rmn, deriv=:ds)
    @test isapprox(x, surface_get(vc, vmecsurf, :r ,deriv=:ds),
                   atol = atol_interp, rtol = rtol_interp)
    x = inverseTransform(vc, vmecsurf.rmn, deriv=:dθ)
    @test isapprox(x, surface_get(vc, vmecsurf, :r ,deriv=:dθ),
                   atol = atol_interp, rtol = rtol_interp)
    x = inverseTransform(vc, vmecsurf.rmn, deriv=:dζ)
    @test isapprox(x, surface_get(vc, vmecsurf, :r ,deriv=:dζ),
                   atol = atol_interp, rtol = rtol_interp)
    x = inverseTransform(vc, vmecsurf.zmn, deriv=:ds)
    @test isapprox(x, surface_get(vc, vmecsurf, :z ,deriv=:ds),
                   atol = atol_interp, rtol = rtol_interp)
    x = inverseTransform(vc, vmecsurf.zmn, deriv=:dθ)
    @test isapprox(x, surface_get(vc, vmecsurf, :z ,deriv=:dθ),
                   atol = atol_interp, rtol = rtol_interp)
    x = inverseTransform(vc, vmecsurf.zmn, deriv=:dζ)
    @test isapprox(x, surface_get(vc, vmecsurf, :z ,deriv=:dζ),
                   atol = atol_interp, rtol = rtol_interp)
    x = inverseTransform(vc, vmecsurf.λmn, deriv=:ds)
    @test isapprox(x, surface_get(vc, vmecsurf, :λ ,deriv=:ds),
                   atol = atol_interp, rtol = rtol_interp)
    x = inverseTransform(vc, vmecsurf.λmn, deriv=:dθ)
    @test isapprox(x, surface_get(vc, vmecsurf, :λ ,deriv=:dθ),
                   atol = atol_interp, rtol = rtol_interp)
    x = inverseTransform(vc, vmecsurf.λmn, deriv=:dζ)
    @test isapprox(x, surface_get(vc, vmecsurf, :λ ,deriv=:dζ),
                   atol = atol_interp, rtol = rtol_interp)
    x = inverseTransform(vc, vmecsurf.gmn, deriv=:ds)
    @test isapprox(x, surface_get(vc, vmecsurf, :g ,deriv=:ds),
                   atol = atol_interp, rtol = rtol_interp)
    x = inverseTransform(vc, vmecsurf.gmn, deriv=:dθ)
    @test isapprox(x, surface_get(vc, vmecsurf, :g ,deriv=:dθ),
                   atol = atol_interp, rtol = rtol_interp)
    x = inverseTransform(vc, vmecsurf.gmn, deriv=:dζ)
    @test isapprox(x, surface_get(vc, vmecsurf, :g ,deriv=:dζ),
                   atol = atol_interp, rtol = rtol_interp)
    x = inverseTransform(vc, vmecsurf.bsubsmn, deriv=:ds)
    @test isapprox(x, surface_get(vc, vmecsurf, :bsubs ,deriv=:ds),
                   atol = atol_interp, rtol = rtol_interp)
    x = inverseTransform(vc, vmecsurf.bsubsmn, deriv=:dθ)
    @test isapprox(x, surface_get(vc, vmecsurf, :bsubs ,deriv=:dθ),
                   atol = atol_interp, rtol = rtol_interp)
    x = inverseTransform(vc, vmecsurf.bsubsmn, deriv=:dζ)
    @test isapprox(x, surface_get(vc, vmecsurf, :bsubs ,deriv=:dζ),
                   atol = atol_interp, rtol = rtol_interp)
    x = inverseTransform(vc, vmecsurf.bsubumn, deriv=:ds)
    @test isapprox(x, surface_get(vc, vmecsurf, :bsubu ,deriv=:ds),
                   atol = atol_interp, rtol = rtol_interp)
    x = inverseTransform(vc, vmecsurf.bsubumn, deriv=:dθ)
    @test isapprox(x, surface_get(vc, vmecsurf, :bsubu ,deriv=:dθ),
                   atol = atol_interp, rtol = rtol_interp)
    x = inverseTransform(vc, vmecsurf.bsubumn, deriv=:dζ)
    @test isapprox(x, surface_get(vc, vmecsurf, :bsubu ,deriv=:dζ),
                   atol = atol_interp, rtol = rtol_interp)
    x = inverseTransform(vc, vmecsurf.bsubvmn, deriv=:ds)
    @test isapprox(x, surface_get(vc, vmecsurf, :bsubv ,deriv=:ds),
                   atol = atol_interp, rtol = rtol_interp)
    x = inverseTransform(vc, vmecsurf.bsubvmn, deriv=:dθ)
    @test isapprox(x, surface_get(vc, vmecsurf, :bsubv ,deriv=:dθ),
                   atol = atol_interp, rtol = rtol_interp)
    x = inverseTransform(vc, vmecsurf.bsubvmn, deriv=:dζ)
    @test isapprox(x, surface_get(vc, vmecsurf, :bsubv ,deriv=:dζ),
                   atol = atol_interp, rtol = rtol_interp)
    x = inverseTransform(vc, vmecsurf.bsupumn, deriv=:ds)
    @test isapprox(x, surface_get(vc, vmecsurf, :bsupu ,deriv=:ds),
                   atol = atol_interp, rtol = rtol_interp)
    x = inverseTransform(vc, vmecsurf.bsupumn, deriv=:dθ)
    @test isapprox(x, surface_get(vc, vmecsurf, :bsupu ,deriv=:dθ),
                   atol = atol_interp, rtol = rtol_interp)
    x = inverseTransform(vc, vmecsurf.bsupumn, deriv=:dζ)
    @test isapprox(x, surface_get(vc, vmecsurf, :bsupu ,deriv=:dζ),
                   atol = atol_interp, rtol = rtol_interp)
    x = inverseTransform(vc, vmecsurf.bsupvmn, deriv=:ds)
    @test isapprox(x, surface_get(vc, vmecsurf, :bsupv ,deriv=:ds),
                   atol = atol_interp, rtol = rtol_interp)
    x = inverseTransform(vc, vmecsurf.bsupvmn, deriv=:dθ)
    @test isapprox(x, surface_get(vc, vmecsurf, :bsupv ,deriv=:dθ),
                   atol = atol_interp, rtol = rtol_interp)
    x = inverseTransform(vc, vmecsurf.bsupvmn, deriv=:dζ)
    @test isapprox(x, surface_get(vc, vmecsurf, :bsupv ,deriv=:dζ),
                   atol = atol_interp, rtol = rtol_interp)
    x = inverseTransform(vc, vmecsurf.currumn, deriv=:ds)
    @test isapprox(x, surface_get(vc, vmecsurf, :curru ,deriv=:ds),
                   atol = atol_interp, rtol = rtol_interp)
    x = inverseTransform(vc, vmecsurf.currumn, deriv=:dθ)
    @test isapprox(x, surface_get(vc, vmecsurf, :curru ,deriv=:dθ),
                   atol = atol_interp, rtol = rtol_interp)
    x = inverseTransform(vc, vmecsurf.currumn, deriv=:dζ)
    @test isapprox(x, surface_get(vc, vmecsurf, :curru ,deriv=:dζ),
                   atol = atol_interp, rtol = rtol_interp)
    x = inverseTransform(vc, vmecsurf.currvmn, deriv=:ds)
    @test isapprox(x, surface_get(vc, vmecsurf, :currv ,deriv=:ds),
                   atol = atol_interp, rtol = rtol_interp)
    x = inverseTransform(vc, vmecsurf.currvmn, deriv=:dθ)
    @test isapprox(x, surface_get(vc, vmecsurf, :currv ,deriv=:dθ),
                   atol = atol_interp, rtol = rtol_interp)
    x = inverseTransform(vc, vmecsurf.currvmn, deriv=:dζ)
    @test isapprox(x, surface_get(vc, vmecsurf, :currv ,deriv=:dζ),
                   atol = atol_interp, rtol = rtol_interp)

  end
  vc = VmecCoordinates(0.7, 0.2, 0.3)
  vc_boundary = VmecCoordinates(0.7, 2π, 2π)
  @testset "Test surface interpolations" begin
    test_splines(vc, vmecsurf)
  end
  @testset "Test surface interpolations, boundary" begin
    test_splines(vc_boundary, vmecsurf)
  end
  @testset "Test exact values" begin
    test_exact(vc, vmecsurf)
  end
  @testset "Test exact values, boundary" begin
    test_exact(vc_boundary, vmecsurf)
  end
  @testset "Test derivatives" begin
    test_derivs(vc, vmecsurf)
  end
  @testset "Test derivatives, boundary" begin
    test_derivs(vc_boundary, vmecsurf)
  end
end
