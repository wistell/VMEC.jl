@testset "VMEC read_vmec_tests" begin
  vmec = VMEC.readVmecWout(joinpath(@__DIR__, "wout_circular_tokamak.nc"));
  rtol = 1.0E-6
  @testset "Test scalar quantities" begin
    @test vmec.nfp == 1
    @test vmec.ns == 17
    @test vmec.mpol == 8
    @test vmec.ntor == 0
    @test vmec.mnmax == 8
    @test vmec.mnmax_nyq == 12
    @test vmec.signgs == -1
    @test isapprox(vmec.aspect,3.0,rtol=rtol)
    @test isapprox(vmec.b0, 5.241673569885196, rtol=rtol)
    @test isapprox(vmec.rmax_surf, 8.0, rtol=rtol)
    @test isapprox(vmec.rmin_surf, 4.0, rtol=rtol)
    @test isapprox(vmec.zmax_surf, 1.9796428837618654, rtol=rtol)
    @test isapprox(vmec.betapol, 0.0, rtol=rtol)
    @test isapprox(vmec.betapol, 0.0, rtol=rtol)
    @test isapprox(vmec.betaxis, 0.0, rtol=rtol)
    @test isapprox(vmec.betatotal, 0.0, rtol=rtol)
    @test isapprox(vmec.Aminor_p, 2.0, rtol=rtol)
    @test isapprox(vmec.Rmajor_p, 6.0, rtol=rtol)
    @test isapprox(vmec.gamma, 0.0, rtol=rtol)
    @test vmec.lfreeb == false
    @test vmec.lasym == false
    @test isapprox(vmec.rbtor, 31.33068006337397, rtol=rtol)
    @test isapprox(vmec.rbtor0,32.14293025734646,rtol=rtol)
    @test isapprox(vmec.IonLarmor, 0.000596984898611916, rtol=rtol)
    @test isapprox(vmec.volavgB, 5.360269593821393, rtol=rtol)
    @test isapprox(vmec.volume_p, 473.74101125228947, rtol=rtol)
    @test isapprox(vmec.wp, 0.0, rtol=rtol)
    @test isapprox(vmec.wb, 172.39494071067705, rtol=rtol)
    @test vmec.pcurr_type == "power_series"
    @test vmec.piota_type == "power_series"
    @test vmec.pmass_type == "power_series"
  end
  @testset "Test vector quantities" begin
    @test isapprox(vmec.xm, collect(0.0:1.0:7.0), rtol=rtol)
    @test isapprox(vmec.xn, zeros(8), rtol=rtol)
    @test isapprox(vmec.raxis_cc, [6.1321884754548845], rtol=rtol)
    @test isapprox(vmec.raxis_cs, zeros(101),rtol=rtol)
    @test isapprox(vmec.zaxis_cs, [0.0], rtol=rtol)
    @test isapprox(vmec.zaxis_cc, zeros(101),rtol=rtol)
    @test isapprox(vmec.am, zeros(21),rtol=rtol)
    @test isapprox(vmec.am_aux_s, zeros(101).-1.0,rtol=rtol)
    @test isapprox(vmec.am_aux_f, zeros(101),rtol=rtol)
    @test isapprox(vmec.ai[1:3], [0.9, -0.65, 0.0],rtol=rtol)
    @test isapprox(vmec.ai_aux_s, zeros(101).-1.0,rtol=rtol)
    @test isapprox(vmec.ai_aux_f, zeros(101),rtol=rtol)
  end
  @testset "Test single extrapolations" begin
    @test isapprox(vmec.phi(0.5), 33.93, rtol=rtol)
    @test isapprox(vmec.chi(0.5), -25.023375, rtol=rtol)
    @test isapprox(vmec.pres(0.5), 0.0, rtol=rtol)
    @test isapprox(vmec.iota(0.5), 0.575, rtol=rtol)
    @test isapprox(vmec.beta_vol(0.5), 0.0, rtol=rtol)
    @test isapprox(vmec.jcuru(0.5), -479425.1642921776, rtol=rtol)
    @test isapprox(vmec.jcurv(0.5), -834820.5411214087, rtol=rtol)
    @test isapprox(vmec.mass(0.5), 0.0, rtol=rtol)
    @test isapprox(vmec.buco(0.5), 1.0584566745010995, rtol=rtol)
    @test isapprox(vmec.bvco(0.5), 31.34357826413739, rtol=rtol)
    @test isapprox(vmec.vp(0.5), 12.153262014071185, rtol=rtol)
    @test isapprox(vmec.specw(0.5), 1.0000055961515837, rtol=rtol)
    @test isapprox(vmec.over_r(0.5), 0.1657696429483258, rtol=rtol)
    @test isapprox(vmec.jdotb(0.5), -2.195083024811319e6, rtol=rtol)
    @test isapprox(vmec.bdotb(0.5), 28.39840645695626, rtol=rtol)
    @test isapprox(vmec.bdotgradv(0.5), 0.8887854041685646, rtol=rtol)
    @test isapprox(vmec.DMerc(0.5), 2.2921470806434484e-5, rtol=rtol)
    @test isapprox(vmec.DShear(0.5), 2.2937126583579245e-5, rtol=rtol)
    @test isapprox(vmec.DCurr(0.5), -1.4925207226334045e-8, rtol=rtol)
    @test isapprox(vmec.DGeod(0.5), -7.305699184311345e-10, rtol=rtol)
    @test isapprox(vmec.equif(0.5), 0.0006218653744368452, rtol=rtol)
  end
  @testset "Test the extrapolation vectors" begin
    @test isapprox(vmec.rmn[2].m, 1.0, rtol=rtol)
    @test isapprox(vmec.rmn[2].n, 0.0, rtol=rtol)
    @test isapprox(vmec.rmn[2].cos(0.5), 1.424492854576644, rtol=rtol)
    @test isapprox(vmec.rmn[2].sin(0.5), 0.0, rtol=rtol)
    @test isapprox(vmec.zmn[2].sin(0.5), 1.4368587464248426, rtol=rtol)
    @test isapprox(vmec.λmn[2].sin(0.5), -0.30669986170239255, rtol=rtol)
    @test isapprox(vmec.bmn[2].cos(0.5), -1.2623409527193068, rtol=rtol)
    @test isapprox(vmec.bsubsmn[2].sin(0.5), 0.08513009063329444, rtol=rtol)
    @test isapprox(vmec.bsubumn[2].cos(0.5), -0.16426462029383657, rtol=rtol)
    @test isapprox(vmec.bsubvmn[2].cos(0.5), -1.657732329709392e-11, rtol=rtol)
    @test isapprox(vmec.bsupumn[2].cos(0.5), -0.07990407977274203, rtol=rtol)
    @test isapprox(vmec.bsupvmn[2].cos(0.5), -0.43171237875416524, rtol=rtol)
    @test isapprox(vmec.currumn[2].cos(0.5), 0.03048595947014148, rtol=rtol)
    @test isapprox(vmec.currvmn[2].cos(0.5), -267247.375000162, rtol=rtol)
  end
end
