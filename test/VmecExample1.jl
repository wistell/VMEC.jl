# This script will show how to load a VMEC file using the Julia interface
# and how to query some quantities of interest
# The first section will load the vmec file and the remaining
# sections will do some basic manipulations of the data


# Set the vmec path. The user will need to change the path below
vmecPath = "/home/abader/VMEC.jl";
# Set the path for the plasma equilibrium toolkit
petPath = "/home/abader/PET.jl";

# Set the path to the vmec WOUT file.  The user will need to change the path below
woutPath = "/home/abader/runs/adelle/wout_RUN12B.00515.nc";

#Add the vmec path and the PET paths
push!(LOAD_PATH,vmecPath); 
push!(LOAD_PATH,petPath);

#load dependencies
using NetCDF; using VMEC;

#load in the raw VMEC data from NETCDF
wout = NetCDF.open(woutPath);

# load the VMEC data into two different formats
# "vmecdata" is a straightforward reconstruction of the vmec wout file but in a julia struct
# "vmec" is a processed form in which all relevant quantities are calculated as splines over the radial coordinate
vmec,vmecdata = VMEC.readVmecWout(wout);

#the stuff included up to this point is necessary for anything below
#the following sections, separated by dashed comment lines
#can be separated out individually and pasted into repl as desired

#--------------------------------------------------------
#Section 1
#Print out some basic quantities from the raw vmec data
#The variable names are the same as in the wout file
#Note currently, not all variables are present
println("Equilibrium path: ",woutPath);
println("Number of periods: ",vmecdata.nfp);
println("Major radius (m): ",vmecdata.Rmajor_p);
println("Minor radius (m): ",vmecdata.Aminor_p);
println("Aspect ratio: ",vmecdata.aspect);
#--------------------------------------------------
#Section 2
#Plot the rot. transform profile for the full and half surfaces from
#the raw vmecdata as a function of s
using Plots

#first construct the full grid in s
#The value at the boundary
phiedge = vmecdata.phi[end];
sfull = vmecdata.phi ./ phiedge;

plot(sfull,vmecdata.iotaf,xaxis="normalized toroidal flux s", label="full grid",
    yaxis="rotational transform",linewidth=3,linecolor=:black )

#to plot the half grid we need to construct a half grid at the midpoints of the full
#grid.  We can exploit that vmec grids are even in normalized flux
shalf = copy(sfull) #if we don't copy, then changing shalf will also change sfull!
sdelta = (sfull[2] - sfull[1])/2.0 #this is half the distance between s points
shalf = shalf .- sdelta #this grid has the points at half s, except for the first point

#both the half grid we constructed and vmecdata.iotas, have a bogus value in the first point
#so we skip that when plotting, the ! tells it to plot on the same plot as before
plot!(shalf[2:end],vmecdata.iotas[2:end],label="half grid", linestyle=:dash,
     linewidth=3,linecolor=:red)

#--------------------------------------------------
#Section 3
#We print out some of the same quantities but using the vmec structure
#This will look nearly identical to section 1
println("Equilibrium path: ",woutPath);
println("Number of periods: ",vmec.nfp);
println("Major radius (m): ",vmec.Rmajor_p);
println("Minor radius (m): ",vmec.Aminor_p);
println("Aspect ratio: ",vmec.aspect);

#--------------------------------------------------
#Section 4
#Plot the rotational transform using the spline format in vmec and compare
#to the raw values

using Plots
#first plot the raw values, these steps are identical to those in section 2
phiedge = vmecdata.phi[end];
sfull = vmecdata.phi ./ phiedge;

plot(sfull,vmecdata.iotaf,xaxis="normalized toroidal flux s", label="full grid",
    yaxis="rotational transform",linewidth=3,linecolor=:black )

#find the ranges of the bspline (note it's not exactly s=0 to s=1
sMin = vmec.iota.itp.ranges[1][1]
sMax = vmec.iota.itp.ranges[1][end]

#construct a high resolution, uniformly spaced s array between these values
nRes = 1000
sHires = LinRange(sMin,sMax,nRes)

#calculate the rot. transform values on the array
iotaHires = vmec.iota[sHires]

#overplot it
plot!(sHires,iotaHires,label="spline", linestyle=:dash,
     linewidth=3,linecolor=:red)

#----------------------------------------------------
#Section 5
#Plot flux surfaces at the full, quarter and half periods directly from the vmec
#data

using Plots
#generate an array of poloidal values from 0-1
npoints = 100
uArray = LinRange(0.0,1.0,npoints)

#normalized toroidal values for each period
vArray = (0, 0.25, 0.5)

#sarray to calculate values at
sArray = (0,0.25,0.5,0.75,1.0)

plot(legend=:none, aspect_ratio=:equal)

#The vmec equilibria are calculated by R = ΣR_mnc * cos(mθ+nϕ) + R_mns * sin(mθ+nϕ)
#where the sum is over the mn values.  the sin term is absent if the equilibrium
#is stellarator symmetric.  The Z term is similar

for v in vArray
  for s in sArray
    global R = zeros(Float64,npoints)
    global Z = zeros(Float64,npoints)
    for i in 1:length(vmec.rmn)
      #define an angle mθ+nϕ = 2π * (mu + nv)
      angle = (vmec.rmn[i][1].*uArray .+ vmec.rmn[i][2]*v) .* 2π

      global R .+= vmec.rmn[i][3](s) .* cos.(angle)
      global Z .+= vmec.zmn[i][3](s) .* sin.(angle)

      #Need to be careful if the equilibrium is not stellarator symmetric
      #todo check that this works properly
      if vmec.lasym
        global R .+= vmec.rmn[i][4](s) .* sin.(angle)
        global Z .+= vmec.zmn[i][4](s) .* cos.(angle)
      end
    end
    plot!(R,Z,linecolor=:blue,linewidth=2)
  end
end
plot!()



#-----------------------------------------------
#Section 6
#generate poincare plots by using higher level functions
#

using Plots

sArray = (0.25, 0.5, 0.75, 0.95);

ntheta = 100

#generate the theta coordinates, note we add a little at the end to make sure
#the surface closes on itself
theta = 0:2*π/ntheta:2*pi+1.0/ntheta
#zeta = 0:π/vmec.nfp/2:π/vmec.nfp;
zeta = (0,π/vmec.nfp/2,π/vmec.nfp);


plot(legend=:none, aspec_ratio=:equal)

#note that julia allows you to loop over multiple variables
for s in sArray, z in zeta

  #calculate information on a specific vmec surface
  local vmecSurf = VmecSurface(s, vmec);

  # generate an array of vmec coordinates
  local fc = VmecCoordinates(s, theta, z);

  # create a list of rzp coordinates
  rzp = map(i->CylindricalFromVmec()(i, vmecSurf),fc);
 
  # the local flag is used to disambugate from R and Z used earlier
  local R = [x.r for x in rzp]
  local Z = [x.z for x in rzp]

  plot!(R,Z,linecolor=:red, linewidth=2)
end

plot!()
