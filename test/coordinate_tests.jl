@testset "Test coordinate transformations" begin
  vmec = VMEC.readVmecWout(joinpath(@__DIR__, "wout_qhs46.nc"));

  srange = range(0.1, 0.9, 5)
  θrange = range(0.01, 2π-0.01, 5)
  ζrange = range(0.01, π/vmec.nfp-0.01, 5)
  rtol = 1.0E-4

  @testset "Test cylindrical transformations" begin
    for s in srange, θ in θrange, ζ in ζrange
      vc = VmecCoordinates(s, θ, ζ)
      cc = CylindricalFromVmec()(vc, vmec)
      vc2 = VmecFromCylindrical()(cc, vmec)
      @test isapprox(vc, vc2, rtol=rtol)
      #only need to test cartesian for a few
      if θ < 0.02 && ζ < 0.02
        xyz = CartesianFromCylindrical()(cc)
        vc3 = VmecFromCartesian()(xyz, vmec)
        @test isapprox(vc, vc3, rtol=rtol)
      end
    end
  end

  @testset "Test flux and Pest transforms" begin
    for s in srange
      vmecsurf = VmecSurface(s, vmec)
      for θ in θrange, ζ in ζrange
        vc = VmecCoordinates(s, θ, ζ)
        fc = FluxFromInternal()(vc, vmecsurf)
        pc = PestFromInternal()(vc, vmecsurf)
        vc2 = InternalFromFlux()(fc, vmecsurf)
        @test isapprox(vc, vc2, rtol=rtol)
        vc3 = InternalFromPest()(pc, vmecsurf)
        @test isapprox(vc, vc3, rtol=rtol)
      end
    end
  end
end
