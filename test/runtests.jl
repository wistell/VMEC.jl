using VMEC
using PlasmaEquilibriumToolkit
using Test
using StructArrays
using CoordinateTransformations
using Interpolations

include("vmec_read_tests.jl")
include("vmec_surface_tests.jl")
include("coordinate_tests.jl")
include("boozer_tests.jl")
