# This script will show how to load a VMEC file using the Julia interface
# and how to query some quantities of interest
# The first section will load the vmec file and the remaining
# sections will calculate epsilon_effective and gamma_c on a given flux surface


# Set the vmec path. The user will need to change the path below
vmecPath = "/home/abader/VMEC.jl";
# Set the path for the plasma equilibrium toolkit
petPath = "/home/abader/PET.jl";
# set the path for Gammac
gammacPath = "/home/abader/GammaC.jl";

# Set the path to the vmec WOUT file.  The user will need to change the path below
#woutPath = "/home/abader/runs/adelle/wout_RUN12B.00515.nc";
#woutPath = "/home/abader/configs/aten/wout_aten_a3b25.nc";
woutPath = "/home/abader/runs/michael/wout_itos.nc"

#Add the vmec path and the PET paths
push!(LOAD_PATH,vmecPath); 
push!(LOAD_PATH,petPath);
push!(LOAD_PATH,gammacPath);


#load dependencies
using NetCDF; using VMEC; using PlasmaEquilibriumToolkit

#include gammac (if gammac is already loaded, you'll get a warning about replacing it)
include(gammacPath*"/GammaC.jl")

#load in the raw VMEC data from NETCDF
wout = NetCDF.open(woutPath);

# load the VMEC data into two different formats
# "vmecdata" is a straightforward reconstruction of the vmec wout file but in a julia struct
# "vmec" is a processed form in which all relevant quantities are calculated as splines over the radial coordinate
vmec,vmecdata = VMEC.readVmecWout(wout);

#the stuff included up to this point is necessary for anything below
#the following sections, separated by dashed comment lines
#can be separated out individually and pasted into repl as desired

#--------------------------------------------------------
#Section 1: 
# calculate eps_effective at a specific flux surface
#
# s surface to calculate on
s = 0.5 

#calculate a surface quantity
vmecSurface = VmecSurface(s, vmec);

# Eps_effective requires two resolution parameters and a length along the field line
# good values for these depend on the config, so some convergence testing is needed
# similar configurations can probably use the same values
lMax = 80*π;
lRes = 2*π/128;
bRes = 100;


eps_eff = GammaC.ComputeGammac(vmecSurface, 0.0, lRes, lMax, bRes, true, false)

println("Epsilon effective: ",eps_eff);
println("Epsilon effective ^ (3/2): ",eps_eff^1.5);

